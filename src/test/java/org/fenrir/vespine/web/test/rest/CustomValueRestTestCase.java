package org.fenrir.vespine.web.test.rest;

import org.junit.Rule;
import org.junit.runner.RunWith;
import com.eclipsesource.restfuse.AuthenticationType;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.RequestContext;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Authentication;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

@RunWith(HttpJUnitRunner.class)
public class CustomValueRestTestCase 
{
	// S'injectarà automàticament després de cada request
	@Context
	private Response response;
	
	@Rule
	public Destination destination = getDestination(); 
	
	private Destination getDestination()
	{
		Destination endpoint = new Destination(this, TestConstants.WS_ENDPOINT);
		
		RequestContext context = endpoint.getRequestContext();
		context.addPathSegment("issueId", "1");
		context.addPathSegment("fieldId", "1");
		
		return endpoint;
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/customField/{fieldId}/customValues.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findCustomFieldValuesTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findFieldValues: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/issue/{issueId}/customValues.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findIssueValuesTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findIssueValues: " + response.getBody());
	}
}
