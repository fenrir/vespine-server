package org.fenrir.vespine.web.test.rest;

import org.junit.Rule;
import org.junit.runner.RunWith;
import com.eclipsesource.restfuse.AuthenticationType;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.RequestContext;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Authentication;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

@RunWith(HttpJUnitRunner.class)
public class TagRestTestCase 
{
	// S'injectarà automàticament després de cada request
	@Context
	private Response response;
	
	@Rule
	public Destination destination = getDestination(); 
	
	private Destination getDestination()
	{
		Destination endpoint = new Destination(this, TestConstants.WS_ENDPOINT);
		
		RequestContext context = endpoint.getRequestContext();
		context.addPathSegment("tagId", "1");
		
		return endpoint;
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/tag/search.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findAllTagsTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findAll: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/tag/search.json?nameFilter=Tag%20preferred&preferred=true",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findPreferredTagsFilteredTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findPreferredTagsFilteredAll: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/tag/search.json?nameFilter=Tag%20preferred",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findTagsFilteredTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findTagsFiltered: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/tag/search.json?preferred=true",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findPreferredTagsTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findPreferredTags: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/tag/search.json?nameFilter=Tag%20proves%202",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findTagsByNameTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findTagsByName: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/tag/{tagId}.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findTagByIdTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findById: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.POST,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/tag",
			content = "name=Tag test 1"
					+ "&preferred=true",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void createTagTest()
	{
		com.eclipsesource.restfuse.Assert.assertCreated(response);
		
		System.out.println("Resposta create: " + response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.PUT,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/tag/{tagId}",
			content = "name=Tag test 1"
					+ "&preferred=true",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void updateTagTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		
		System.out.println("Resposta update: " + response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.DELETE,
			path = "/rest/tag/{tagId}",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) },
			// S'ha d'executar l'últim
			order = 1
	)
	public void deleteTagTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);		
	}
}
