package org.fenrir.vespine.web.test.rest;

import org.junit.Rule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.eclipsesource.restfuse.AuthenticationType;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Authentication;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

/**
 * TestSuite per provar la funcionalitat de login a través de la interficie REST de l'aplicació
 * @author Antonio Archilla Nava
 * @version v0.1.201409013
 */
@RunWith(HttpJUnitRunner.class)
public class AuthenticationRestTestCase 
{
	private final Logger log = LoggerFactory.getLogger(AuthenticationRestTestCase.class);
	
	// S'injectarà automàticament després de cada request
	@Context
	private Response response;
	
	@Rule
	public Destination destination = getDestination(); 
	
	private Destination getDestination()
	{
		Destination endpoint = new Destination(this, TestConstants.WS_ENDPOINT);
		
		return endpoint;
	}
	
	@HttpTest( 
			method = Method.POST,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/authentication/login",
			content = "dummy",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void loginOkTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		log.debug("Responsta login: {}", response.getBody());
	}
	
	@HttpTest( 
			method = Method.POST,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/authentication/login",
			content = "dummy",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = "incorrectUser", password = "incorrectUser") } 
	)
	public void loginFailureTest()
	{
		com.eclipsesource.restfuse.Assert.assertUnauthorized(response);
		log.debug("Responsta login: {}", response.getBody());
	}
}
