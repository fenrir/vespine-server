package org.fenrir.vespine.web.test.rest;

import org.junit.Rule;
import org.junit.runner.RunWith;

import com.eclipsesource.restfuse.AuthenticationType;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.RequestContext;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Authentication;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

@RunWith(HttpJUnitRunner.class)
public class ViewFilterRestTestCase 
{
	// S'injectarà automàticament després de cada request
	@Context
	private Response response;
	
	@Rule
	public Destination destination = getDestination(); 
	
	private Destination getDestination()
	{
		Destination endpoint = new Destination(this, TestConstants.WS_ENDPOINT);
		
		RequestContext context = endpoint.getRequestContext();
		context.addPathSegment("filterId", "1");
		
		return endpoint;
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/viewFilter/search.json?applicationOnly=true",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findApplicationFiltersTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findApplicationFilters: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/viewFilter/search.json?user=basicUser",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findUserFiltersTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findUserFilters: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/viewFilter/{filterId}.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findFilterByIdTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findById: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.POST,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/viewFilter",
			content = "name=Filtre proves"
					+ "&description=Filtre de proves"
					+ "&query=from issue"
					+ "&orderByClause=",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void createFilterTest()
	{
		com.eclipsesource.restfuse.Assert.assertCreated(response);
		
		System.out.println("Resposta create: " + response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.PUT,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/viewFilter/{filterId}",
			content = "name=Filtre proves MOD"
					+ "&description=Filtre de proves MOD"
					+ "&query=from issue"
					+ "&orderByClause=",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void updateFilterTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		
		System.out.println("Resposta update: " + response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.PUT,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/viewFilter/{filterId}",
			content = "order=1",					
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void reorderFilterTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		
		System.out.println("Resposta reorder: " + response.getHeaders().get("Location"));
	}
}
