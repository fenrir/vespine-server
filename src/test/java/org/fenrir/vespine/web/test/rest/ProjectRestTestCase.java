package org.fenrir.vespine.web.test.rest;

import org.junit.Rule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.eclipsesource.restfuse.AuthenticationType;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.RequestContext;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;
import com.eclipsesource.restfuse.annotation.Authentication;

@RunWith(HttpJUnitRunner.class)
public class ProjectRestTestCase 
{
	private final Logger log = LoggerFactory.getLogger(ProjectRestTestCase.class);
	
	// S'injectarà automàticament després de cada request
	@Context
	private Response response;
	
	@Rule
	public Destination destination = getDestination(); 
	
	private Destination getDestination()
	{
		Destination endpoint = new Destination(this, TestConstants.WS_ENDPOINT);
		
		RequestContext context = endpoint.getRequestContext();
		context.addPathSegment("workflowId", "1");
		context.addPathSegment("projectId", "1");
		
		return endpoint;
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/project/search.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findAllProjectsTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		log.debug("Resposta findAll: {}", response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/project/search.json?workflow={workflowId}",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findProjectsByWorkflowTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		log.debug("Resposta findByWorkflow: {}", response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/project/{projectId}.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findProjectByIdTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		log.debug("Resposta findById: {}", response.getBody());
	}
	
	@HttpTest( 
			method = Method.POST,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/project",
			content = "name=Projecte de proves"
					+ "&workflow=1"
					+ "&abbreviation=PR",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void createProjectTest()
	{
		com.eclipsesource.restfuse.Assert.assertCreated(response);
		
		log.debug("Resposta create: {}", response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.PUT,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/project/{projectId}",
			content = "name=Projecte 1 Modificat"
					+ "&workflow=1"
					+ "&abbreviation=MOD",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void updateProjectTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		
		log.debug("Resposta update: {}", response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.DELETE,
			path = "/rest/project/{projectId}",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) },
			// S'ha d'executar l'últim
			content = "originator=1111-1111-1111",
			order = 1
	)
	public void deleteProjectTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);		
	}
}
