package org.fenrir.vespine.web.test.rest;

public class TestConstants 
{
	public static final String WS_ENDPOINT = "http://localhost:8080/vespine";
	
	public static final String CREDENTIALS_ADMIN_USERNAME = "admin";
	public static final String CREDENTIALS_ADMIN_PASSWORD = "admin";
	public static final String CREDENTIALS_OBSERVER_USERNAME = "observer";
	public static final String CREDENTIALS_OBSERVER_PASSWORD = "observer";
	public static final String CREDENTIALS_REPORTER_USERNAME = "reporter";
	public static final String CREDENTIALS_REPORTER_PASSWORD = "reporter";
}
