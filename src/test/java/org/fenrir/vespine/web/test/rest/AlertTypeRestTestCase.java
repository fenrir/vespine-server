package org.fenrir.vespine.web.test.rest;

import org.junit.Rule;
import org.junit.runner.RunWith;
import com.eclipsesource.restfuse.AuthenticationType;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.RequestContext;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Authentication;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

@RunWith(HttpJUnitRunner.class)
public class AlertTypeRestTestCase 
{
	// S'injectarà automàticament després de cada request
	@Context
	private Response response;
	
	@Rule
	public Destination destination = getDestination(); 
	
	private Destination getDestination()
	{
		Destination endpoint = new Destination(this, TestConstants.WS_ENDPOINT);
		
		RequestContext context = endpoint.getRequestContext();
		context.addPathSegment("alertTypeId", "1");
		
		return endpoint;
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/alertType/search.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) },
			order = 1
	)
	public void findAllAlertTypesTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findAll: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/alertType/{alertTypeId}.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) },
			order = 1
	)
	public void findAlertTypeByIdTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findById: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.POST,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/alertType",
			content = "name=Tipus d'alerta 1"
					+ "&description=Tipus d'alerta 1"
					+ "&alertRule=true;"
					+ "&generateOld=true",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void createAlertTypeTest()
	{
		com.eclipsesource.restfuse.Assert.assertCreated(response);
		
		System.out.println("Resposta create: " + response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.PUT,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/alertType/{alertTypeId}",
			content = "name=Tipus d'alerta 1 Modificada"
					+ "&description=Tipus d'alerta 1 Modificada"
					+ "&alertRule=true;",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) },
			order = 1
	)
	public void updateAlertTypeTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		
		System.out.println("Resposta update: " + response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.DELETE,
			path = "/rest/alertType/{alertTypeId}",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) },
			order = 2
	)
	public void deleteAlertTypeTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);		
	}
}
