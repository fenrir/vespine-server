package org.fenrir.vespine.web.test.rest;

import org.junit.Rule;
import org.junit.runner.RunWith;
import com.eclipsesource.restfuse.AuthenticationType;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.RequestContext;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Authentication;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

@RunWith(HttpJUnitRunner.class)
public class CustomFieldRestTestCase 
{
	// S'injectarà automàticament després de cada request
	@Context
	private Response response;
	
	@Rule
	public Destination destination = getDestination(); 
	
	private Destination getDestination()
	{
		Destination endpoint = new Destination(this, TestConstants.WS_ENDPOINT);
		
		RequestContext context = endpoint.getRequestContext();
		context.addPathSegment("fieldId", "1");
		context.addPathSegment("projectId", "1");
		
		return endpoint;
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/customField/types.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findFieldTypes()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findFieldTypes: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/customField/search.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findAllCustomFields()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findAll: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/customField/search.json?projectId={projectId}",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findCustomFieldsByProject()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findByProject: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/customField/{fieldId}.json",
			authentications= { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) } 
	)
	public void findCustomFieldByIdTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
		
		System.out.println("Resposta findById: " + response.getBody());
	}
	
	@HttpTest( 
			method = Method.POST,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/customField",
			content = "name=Camp de proves"
					+ "&fieldType=STRING"
					+ "&dataProvider="
					+ "&mandatory=true"
					+ "&project=1",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void createWorkflowTest()
	{
		com.eclipsesource.restfuse.Assert.assertCreated(response);
		
		System.out.println("Resposta create: " + response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.PUT,
			type = MediaType.APPLICATION_FORM_URLENCODED,
			path = "/rest/customField/{fieldId}",
			content = "name=Camp de proves MOD"
					+ "&fieldType=STRING"
					+ "&dataProvider="
					+ "&mandatory=true"
					+ "&project=1",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) } 
	)
	public void updateWorkflowTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);
		
		System.out.println("Resposta update: " + response.getHeaders().get("Location"));
	}
	
	@HttpTest( 
			method = Method.DELETE,
			path = "/rest/customField/{fieldId}",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_ADMIN_USERNAME, password = TestConstants.CREDENTIALS_ADMIN_PASSWORD) },
			// S'ha d'executar l'últim
			order = 1
	)
	public void deleteCustomFieldTest()
	{
		com.eclipsesource.restfuse.Assert.assertOk(response);		
	}
}
