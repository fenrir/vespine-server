package org.fenrir.vespine.web.test.rest;

import org.junit.Rule;
import org.junit.runner.RunWith;
import com.eclipsesource.restfuse.AuthenticationType;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.MediaType;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Authentication;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

@RunWith(HttpJUnitRunner.class)
public class ErrorReponseTestCase 
{
	// S'injectarà automàticament després de cada request
	@Context
	private Response response;
	
	@Rule
	public Destination destination = getDestination(); 
	
	private Destination getDestination()
	{
		Destination endpoint = new Destination(this, TestConstants.WS_ENDPOINT);
		return endpoint;
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/not/valid/url.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) }			
	)
	public void notValidUrlTest()
	{
		// 500
		com.eclipsesource.restfuse.Assert.assertInternalServerError(response);
		org.junit.Assert.assertEquals(MediaType.TEXT_PLAIN, response.getType());
		
		org.junit.Assert.assertEquals("null for uri: http://localhost:8080/vespine/rest/not/valid/url.json", response.getBody());
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/project/json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) }			
	)
	public void notValidUrl2Test()
	{
		// 500
		com.eclipsesource.restfuse.Assert.assertInternalServerError(response);
		org.junit.Assert.assertEquals(MediaType.TEXT_PLAIN, response.getType());
		
		org.junit.Assert.assertEquals("Error inesperat", response.getBody());		
	}
	
	@HttpTest( 
			method = Method.GET, 
			path = "/rest/project/9999.json",
			authentications = { @Authentication(type = AuthenticationType.BASIC, user = TestConstants.CREDENTIALS_OBSERVER_USERNAME, password = TestConstants.CREDENTIALS_OBSERVER_PASSWORD) }			
	)
	public void notFoundTest()
	{
		// 204
		com.eclipsesource.restfuse.Assert.assertNoContent(response);
		org.junit.Assert.assertEquals(MediaType.APPLICATION_JSON, response.getType());
	}
}
