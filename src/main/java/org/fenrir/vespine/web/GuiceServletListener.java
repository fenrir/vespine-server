package org.fenrir.vespine.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import org.apache.shiro.guice.aop.ShiroAopModule;
import org.fenrir.vespine.web.module.ApplicationModule;
import org.fenrir.vespine.web.module.BroadcastModule;
import org.fenrir.vespine.web.module.RestModule;
import org.fenrir.vespine.web.module.ServletConfigurationModule;
import org.fenrir.vespine.web.module.ShiroSecurityModule;
import org.fenrir.vespine.web.module.UserDBDatasourceModule;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140908
 */
public class GuiceServletListener extends GuiceServletContextListener  
{
	private ServletContext servletContext;
	
	@Override
    public void contextInitialized(ServletContextEvent servletContextEvent)
	{
		this.servletContext = servletContextEvent.getServletContext();
        super.contextInitialized(servletContextEvent);
    }
	
	@Override
    protected Injector getInjector() 
    {
		/* A la inicialització del framework Guice s'indica que s'utilitzaran els mòduls REST, 
		 * JPA, seguretat Shiro i la configuració dels components de l'aplicació
		 */
		Injector injector = Guice.createInjector(new ApplicationModule(),
				new UserDBDatasourceModule(),
				new RestModule(),
				new BroadcastModule(),
				new ServletConfigurationModule(),
				new ShiroSecurityModule(servletContext),
				// Necessari per interceptar les anotacions de configuració de Shiro a través de Guice
				new ShiroAopModule());
		ApplicationContext.getInstance().initialize(injector);
        
        return injector;
    }
}
