package org.fenrir.vespine.web.service.impl;

import java.util.Collection;
import java.util.Map;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140815
 */
public class AtmosphereBroadcastServiceImpl implements IBroadcastService<AtmosphereResource> 
{
	private final Logger log = LoggerFactory.getLogger(AtmosphereBroadcastServiceImpl.class);
	
	@Override
	public void subscribe(String username, AtmosphereResource resource)
	{
		// Canal aplicació
		Broadcaster applicationBroadcaster = BroadcasterFactory.getDefault().lookup(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, true);
		applicationBroadcaster.addAtmosphereResource(resource);
	}
	
	@Override
	public void sendMessage(Object message) throws BusinessException
	{
		Collection<Broadcaster> broadcasters = BroadcasterFactory.getDefault().lookupAll();
		try{
			String strMessage = null;
        	if(message instanceof String){
        		strMessage = (String)message;
        	}
        	else{
        		strMessage = new ObjectMapper().writeValueAsString(message);
        	}
        	for(Broadcaster broadcaster:broadcasters){
    			broadcaster.broadcast(strMessage);
    		}
        }
        catch(Exception e){
        	throw new BusinessException("Error enviant missatge: " + e.getMessage(), e);
        }
	}
	
	@Override
	public void sendMessage(String receiverId, Object message) throws BusinessException
	{
        Broadcaster broadcaster = BroadcasterFactory.getDefault().lookup(receiverId);
//      logger.debug("thread: {} SEND to '{}' from {}: {}", new Object[]{Thread.currentThread().getName(), broadcaster.getID(), from, message});
        log.debug("thread: {} SEND to '{}': {}", new Object[]{Thread.currentThread().getName(), broadcaster.getID(), message});

        try{
        	String strMessage = null;
        	if(message instanceof String){
        		strMessage = (String)message;
        	}
        	else{
        		strMessage = new ObjectMapper().writeValueAsString(message);
        	}
        	broadcaster.broadcast(strMessage);
        }
        catch(Exception e){
        	throw new BusinessException("Error enviant missatge: " + e.getMessage(), e);
        }
	}
	
	@Override
	public void sendMessage(String receiverId, String originatorId, String type, String message) throws BusinessException
	{
		sendMessage(receiverId, originatorId, type, message, null);
	}
	
	@Override
	public void sendMessage(String receiverId, String originatorId, String type, String message, Map<String, Object> extraData) throws BusinessException
	{
		BroadcastMessage objMessage = new BroadcastMessage();
		objMessage.setOriginator(originatorId);
		objMessage.setType(type);
		objMessage.setMessage(message);
		objMessage.setExtraData(extraData);
		
		sendMessage(receiverId, objMessage);
	}
}
