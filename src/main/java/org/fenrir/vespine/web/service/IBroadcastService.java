package org.fenrir.vespine.web.service;

import java.util.Map;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140913
 */
public interface IBroadcastService<T> 
{
	public void subscribe(String username, T resource);
	
	public void sendMessage(Object message) throws BusinessException;
	public void sendMessage(String receiverId, Object message) throws BusinessException;
	public void sendMessage(String receiverId, String originatorId, String type, String message) throws BusinessException;
	public void sendMessage(String receiverId, String originatorId, String type, String message, Map<String, Object> extraData) throws BusinessException;
}
