package org.fenrir.vespine.web.module;

import java.util.Collections;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.google.inject.TypeLiteral;

import org.atmosphere.guice.AtmosphereGuiceServlet;
import org.fenrir.vespine.web.controller.async.SendMessageHandler;
import org.fenrir.vespine.web.controller.async.SubscriptionHandler;
import org.fenrir.vespine.web.service.IBroadcastService;
import org.fenrir.vespine.web.service.impl.AtmosphereBroadcastServiceImpl;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.201409013
 */
public class BroadcastModule extends AbstractModule 
{
	@Override
	protected void configure() 
	{
		// Handlers d'atmosphere
		bind(SubscriptionHandler.class).in(Singleton.class);
		bind(SendMessageHandler.class).in(Singleton.class);
		
		// Services
		bind(IBroadcastService.class).to(AtmosphereBroadcastServiceImpl.class).in(Singleton.class);
		
		bind(new TypeLiteral<Map<String, String>>(){  })
				.annotatedWith(Names.named(AtmosphereGuiceServlet.PROPERTIES))
				.toInstance(Collections.<String, String>emptyMap());
	}
}
