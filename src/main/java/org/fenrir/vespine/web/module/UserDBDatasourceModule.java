package org.fenrir.vespine.web.module;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;
import com.google.inject.Key;
import com.google.inject.PrivateModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.security.dao.IPermissionDAO;
import org.fenrir.vespine.web.security.dao.IRoleDAO;
import org.fenrir.vespine.web.security.dao.IUserDAO;
import org.fenrir.vespine.web.security.dao.impl.PermissionDAOImpl;
import org.fenrir.vespine.web.security.dao.impl.RoleDAOImpl;
import org.fenrir.vespine.web.security.dao.impl.UserDAOImpl;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140728
 */
public class UserDBDatasourceModule extends PrivateModule 
{
	public static final Key<PersistFilter> KEY_PERSIST_FILTER_USERS = Key.get(PersistFilter.class, Names.named(ApplicationConstants.PERSISTENCE_UNIT_USERS));
	
	private final Logger log = LoggerFactory.getLogger(UserDBDatasourceModule.class);
	
	@Override
	protected void configure() 
	{
		Properties applicationProperties = loadApplicationProperties();
		
		/* DAOs */
		bind(IUserDAO.class).to(UserDAOImpl.class).in(Singleton.class);
		expose(IUserDAO.class);
		bind(IRoleDAO.class).to(RoleDAOImpl.class).in(Singleton.class);
		expose(IRoleDAO.class);
		bind(IPermissionDAO.class).to(PermissionDAOImpl.class).in(Singleton.class);
		expose(IPermissionDAO.class);
		
		/* Mòduls addicionals */
		// Persistencia
		Properties persistenceModuleProperties = new Properties();
		String applicationDatabase = applicationProperties.getProperty(ApplicationConstants.CONFIGURATION_USER_DATABASE_URL);
        persistenceModuleProperties.put("hibernate.connection.datasource", applicationDatabase);
//        persistenceModuleProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        persistenceModuleProperties.put("hibernate.dialect_resolvers", "org.hibernate.dialect.resolver.StandardDialectResolver");
        // ATENCIÓ!! Habilitar només per debug
//        persistenceModuleProperties.put("hibernate.show_sql", "true");
        /* value='create' per crear una nova base de dades en cada execució;
         * value='update' per modificar una ja existent;
         * value='create-drop' igual que 'create' però eliminant les taules quan acaba l'execució;
         * value='validate' no modifica la base de dades
         */
        persistenceModuleProperties.put("hibernate.hbm2ddl.auto", "validate");
        persistenceModuleProperties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        // S'anula la detecció d'entitats automàtiques. El seu valor per defecte és "class"
        persistenceModuleProperties.put("hibernate.archive.autodetection", "");
        JpaPersistModule persistenceModule = new JpaPersistModule(ApplicationConstants.PERSISTENCE_UNIT_USERS);
        persistenceModule.properties(persistenceModuleProperties);
        install(persistenceModule);
        
        bind(KEY_PERSIST_FILTER_USERS).to(PersistFilter.class);
        expose(KEY_PERSIST_FILTER_USERS);
	}
	
	private Properties loadApplicationProperties()
	{
		Properties properties = new Properties();
		try{
			FileInputStream fis = new FileInputStream(new File(getClass().getResource("/org/fenrir/vespine/web/conf/application.properties").toURI()));
			properties.load(fis);
			
			fis.close();
		}		
		catch(IOException e){
			// No s'hauria de donar mai
			log.error("Error en llegir les propietats d'arrancada: " + e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		catch(URISyntaxException e){
			// No s'hauria de donar mai
			log.error("Error en llegir les propietats d'arrancada: " + e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		
		return properties;
	}
}
