package org.fenrir.vespine.web.module;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import org.fenrir.vespine.web.controller.rest.AlertTypeController;
import org.fenrir.vespine.web.controller.rest.ApplicationController;
import org.fenrir.vespine.web.controller.rest.AuthenticationController;
import org.fenrir.vespine.web.controller.rest.CategoryController;
import org.fenrir.vespine.web.controller.rest.CustomFieldController;
import org.fenrir.vespine.web.controller.rest.DictionaryController;
import org.fenrir.vespine.web.controller.rest.IndexController;
import org.fenrir.vespine.web.controller.rest.ProjectController;
import org.fenrir.vespine.web.controller.rest.ProviderController;
import org.fenrir.vespine.web.controller.rest.RestExceptionMapper;
import org.fenrir.vespine.web.controller.rest.SeverityController;
import org.fenrir.vespine.web.controller.rest.StatusController;
import org.fenrir.vespine.web.controller.rest.TagController;
import org.fenrir.vespine.web.controller.rest.UnauthorizedExceptionMapper;
import org.fenrir.vespine.web.controller.rest.UserController;
import org.fenrir.vespine.web.controller.rest.ViewFilterController;
import org.fenrir.vespine.web.controller.rest.WorkflowController;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140908
 */
public class RestModule extends AbstractModule
{
	@Override
	protected void configure() 
	{
		/* Els controladors REST de Jersey que contingin anotacions del tipus 
		 * RequiresRoles / permissions de Shiro s'hauran de lligar manualment 
		 * encara que s'hagi configurat l'scan de packages perquè requereixen 
		 * un interceptor. 
		 */
		bind(AuthenticationController.class).in(Singleton.class);
    	bind(ApplicationController.class).in(Singleton.class);
    	bind(ProjectController.class).in(Singleton.class);
    	bind(CategoryController.class).in(Singleton.class);
    	bind(SeverityController.class).in(Singleton.class);
    	bind(StatusController.class).in(Singleton.class);
    	bind(WorkflowController.class).in(Singleton.class);
    	bind(AlertTypeController.class).in(Singleton.class);
    	bind(TagController.class).in(Singleton.class);
    	bind(DictionaryController.class).in(Singleton.class);
    	bind(CustomFieldController.class).in(Singleton.class);
    	bind(ProviderController.class).in(Singleton.class);
    	bind(ViewFilterController.class).in(Singleton.class);
    	bind(IndexController.class).in(Singleton.class);
    	bind(UserController.class).in(Singleton.class);
    	
    	// En necessari incloure els recursos REST per tal que siguin visibles per RestEasy a través de Guice
    	bind(RestExceptionMapper.class);
    	bind(UnauthorizedExceptionMapper.class);
	}
}
