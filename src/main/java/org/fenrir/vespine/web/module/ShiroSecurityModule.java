package org.fenrir.vespine.web.module;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import org.apache.shiro.authc.AuthenticationListener;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.guice.web.ShiroWebModule;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.AbstractMatcher;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.security.listener.AccessListener;
import org.fenrir.vespine.web.security.realm.AnnotatedJdbcRealm;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140829
 */
public class ShiroSecurityModule extends ShiroWebModule 
{
	private final Logger log = LoggerFactory.getLogger(ShiroSecurityModule.class);
	
	public ShiroSecurityModule(ServletContext context) 
    {
        super(context);
    }
	
	@SuppressWarnings("unchecked")
	protected void configureShiroWeb() 
	{
		// Realm a utilitzar per dur a terme la autenticació / autorització
		bindRealm().to(AnnotatedJdbcRealm.class);
		
		/* Query per dur a terme l'autenticació de l'usuari. Si no s'indica, per defecte s'assumeix 
		 * "select password from users where username = ?"
		 */
		bindConstant().annotatedWith(Names.named("shiro.authenticationQuery")).to("select PASSWORD from APP_USER where NAME = ?");
		/* Query per recuperar els rols de l'usuari. Si no s'indica, per defecte s'assumeix 
		 * "select role_name from user_roles where username = ?"
		 */
		bindConstant().annotatedWith(Names.named("shiro.userRolesQuery")).to("select APP_ROLE.NAME "
				+ "from USER_ROLES, APP_USER, APP_ROLE "
				+ "where APP_USER.ID=USER_ROLES.USER_ID and APP_ROLE.ID=USER_ROLES.ROLE_ID and APP_USER.NAME = ?");
		/* Query per recuperar els permisos associats als rols de l'usuari. Si no s'indica, per defecte s'assumeix
		 * "select permission from roles_permissions where role_name = ?"
		 */
		bindConstant().annotatedWith(Names.named("shiro.permissionsQuery")).to("select APP_PERMISSION.VALUE "
				+ "from APP_PERMISSION, APP_ROLE, ROLE_PERMISSIONS "
				+ "where APP_ROLE.ID=ROLE_PERMISSIONS.ROLE_ID and APP_PERMISSION.ID=ROLE_PERMISSIONS.PERMISSION_ID and APP_ROLE.NAME = ?");
		
		// El realm també s'encarregarà de l'autorització
		bindConstant().annotatedWith(Names.named("shiro.permissionsLookupEnabled")).to(Boolean.TRUE);
		
		bindConstant().annotatedWith(Names.named("shiro.loginUrl")).to("/rest/authentication/login");
		
		// Listener accessos
		bindListener(new AbstractMatcher<TypeLiteral<?>>() 
        {
            @Override
            public boolean matches(TypeLiteral<?> typeLiteral) 
            {
                return DefaultWebSecurityManager.class.isAssignableFrom(typeLiteral.getRawType());
            }
        }, new SecurityManagerTypeListener());
				
		// URLs Login
		addFilterChain("/rest/authentication/**", AUTHC_BASIC);
		// URLs administració elements de l'aplicació
        addFilterChain("/rest/project/**", AUTHC_BASIC);
        addFilterChain("/rest/category/**", AUTHC_BASIC);
        addFilterChain("/rest/severity/**", AUTHC_BASIC);
        addFilterChain("/rest/status/**", AUTHC_BASIC);
        addFilterChain("/rest/workflow/**", AUTHC_BASIC);
        addFilterChain("/rest/alertType/**", AUTHC_BASIC);
        addFilterChain("/rest/tag/**", AUTHC_BASIC);
        addFilterChain("/rest/dictionary/**", AUTHC_BASIC);
        addFilterChain("/rest/customField/**", AUTHC_BASIC);
        addFilterChain("/rest/provider/**", AUTHC_BASIC);
        addFilterChain("/rest/sprint/**", AUTHC_BASIC);
        addFilterChain("/rest/viewFilter/**", AUTHC_BASIC);
        addFilterChain("/rest/index/**", AUTHC_BASIC);
        addFilterChain("/rest/user/**", AUTHC_BASIC);
        // Gestió d'incidencies 
        addFilterChain("/rest/issue/**", AUTHC_BASIC);
        addFilterChain("/rest/**", ANON);
        // URLs de broadcast
        addFilterChain("/broadcast/**", AUTHC_BASIC);
    }

	@Provides
	@Singleton
	DataSource getDataSource(@Named(ApplicationConstants.NAMED_INJECTION_APPLICATION_PROPERTIES)Properties applicationProperties) throws Exception
	{
		// S'accedirà al Datasource a través de JNDI
		String dbUrl = applicationProperties.getProperty(ApplicationConstants.CONFIGURATION_USER_DATABASE_URL);
		InitialContext cxt = new InitialContext();
		DataSource datasource = (DataSource)cxt.lookup(dbUrl);
		if(datasource==null){
		   throw new Exception("Error localitzant el datasource de la base de dades d'usuaris");
		}
		
		return datasource;
	}
	
	/**
	 * Especificació del password hashing
	 * @return CredentialsMatcher
	 */
	@Provides
	@Singleton
	CredentialsMatcher getCredentialsMatcher()
	{
		HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
		matcher.setHashAlgorithmName(Sha256Hash.ALGORITHM_NAME);
		// Important si es guarden els password en format HEX
		matcher.setStoredCredentialsHexEncoded(true);
		// Iteracions per defecte
		matcher.setHashIterations(500000);
		
		return matcher;
	}
	
	class SecurityManagerTypeListener implements TypeListener
    {
        @Override
        public <I> void hear(final TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) 
        {
            typeEncounter.register(new InjectionListener<I>() 
            {
                /**
                 * Mètode executat una vegada Guice ha injectat tots els membres a l'objecte
                 */
                @Override
                public void afterInjection(I i) 
                {
                    if(log.isDebugEnabled()){
                        log.debug("Executant post inicialització del SecurityManager {}", i.getClass().getName());
                    }
                    
                    DefaultWebSecurityManager manager = (DefaultWebSecurityManager)i;
                    Set<AuthenticationListener> listeners = new HashSet<AuthenticationListener>();
                    listeners.add(new AccessListener());
                    ((ModularRealmAuthenticator)manager.getAuthenticator()).setAuthenticationListeners(listeners);
                }
            });
        }
    }
}