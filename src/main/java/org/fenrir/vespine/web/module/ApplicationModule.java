package org.fenrir.vespine.web.module;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Properties;
import com.google.inject.Key;
import com.google.inject.PrivateModule;
import com.google.inject.name.Names;
import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.spi.util.BooleanConverter;
import org.fenrir.vespine.spi.util.DateConverter;
import org.fenrir.vespine.spi.util.IntegerConverter;
import org.fenrir.vespine.spi.util.StringConverter;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.ApplicationContext;
import org.fenrir.vespine.core.entity.converter.DictionaryItemConverter;
import org.fenrir.vespine.core.entity.converter.IssueCategoryConverter;
import org.fenrir.vespine.core.entity.converter.IssueProjectConverter;
import org.fenrir.vespine.core.entity.converter.IssueSeverityConverter;
import org.fenrir.vespine.core.entity.converter.IssueStatusConverter;
import org.fenrir.vespine.core.entity.converter.SprintConverter;
import org.fenrir.vespine.core.entity.converter.UserConverter;
import org.fenrir.vespine.core.module.VespineCoreModule;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.service.IIssueWorkService;
import org.fenrir.vespine.core.service.IProviderAdministrationService;
import org.fenrir.vespine.core.service.ISearchIndexService;
import org.fenrir.vespine.core.service.IUserManagementService;
import org.fenrir.vespine.core.service.IWorkflowService;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140908
 */
public class ApplicationModule extends PrivateModule
{
	public static final Key<PersistFilter> KEY_PERSIST_FILTER_APPLICATION = Key.get(PersistFilter.class, Names.named(ApplicationConstants.PERSISTENCE_UNIT_APPLICATION));
	
	private final Logger log = LoggerFactory.getLogger(ApplicationModule.class);
	
	@Override
	protected void configure()
	{
		Properties applicationProperties = loadApplicationProperties();
		
		/* Mòduls addicionals */
		// Persistencia
		Properties persistenceModuleProperties = new Properties();
		String applicationDatabase = applicationProperties.getProperty(ApplicationConstants.CONFIGURATION_DATA_DATABASE_URL);
        persistenceModuleProperties.put("hibernate.connection.datasource", applicationDatabase);
//        persistenceModuleProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        persistenceModuleProperties.put("hibernate.dialect_resolvers", "org.hibernate.dialect.resolver.StandardDialectResolver");
        // ATENCIÓ!! Habilitar només per debug
//        persistenceModuleProperties.put("hibernate.show_sql", "true");
        /* value='create' per crear una nova base de dades en cada execució;
         * value='update' per modificar una ja existent;
         * value='create-drop' igual que 'create' però eliminant les taules quan acaba l'execució;
         * value='validate' no modifica la base de dades
         */
        persistenceModuleProperties.put("hibernate.hbm2ddl.auto", "validate");
        persistenceModuleProperties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        // S'anula la detecció d'entitats automàtiques. El seu valor per defecte és "class"
        persistenceModuleProperties.put("hibernate.archive.autodetection", "");
        // Listeners lifecycle Hibernate
        persistenceModuleProperties.put("hibernate.ejb.event.post-insert", "org.fenrir.vespine.core.entity.listener.TrackedFieldsListener, " +
        		"org.fenrir.vespine.core.entity.listener.IssueProjectListener");
        persistenceModuleProperties.put("hibernate.ejb.event.post-update", "org.fenrir.vespine.core.entity.listener.TrackedFieldsListener");
        persistenceModuleProperties.put("hibernate.ejb.event.post-delete", "org.fenrir.vespine.core.entity.listener.IssueProjectListener");
        JpaPersistModule persistenceModule = new JpaPersistModule(ApplicationConstants.PERSISTENCE_UNIT_APPLICATION);
        persistenceModule.properties(persistenceModuleProperties);
        install(persistenceModule);
        
        bind(KEY_PERSIST_FILTER_APPLICATION).to(PersistFilter.class);
        expose(KEY_PERSIST_FILTER_APPLICATION);
        
		// Mòdul Core
		HashMap<String, Object> coreModuleProperties = new HashMap<String, Object>();
		coreModuleProperties.put(VespineCoreModule.PROPERTY_DATA_PERSISTENCE_UNIT, ApplicationConstants.PERSISTENCE_UNIT_APPLICATION);
		coreModuleProperties.put(VespineCoreModule.PROPERTY_WORKSPACE_FOLDER, ApplicationContext.getInstance().getWorkspaceLocation());
		coreModuleProperties.put(VespineCoreModule.PROPERTY_INDEX_STORE, VespineCoreModule.PROPERTY_INDEX_STORE_FILESYSTEM_VALUE);
		install(new VespineCoreModule(coreModuleProperties));
		
		// S'han d'exposar els serveis que s'utilitzen dins de Vespine-Server. En el cas dels controladors, encara que estiguin definits al mòdul no es poden injectar aquests membres per ser creats de forma externa
		expose(ICoreAdministrationService.class);
		expose(IIssueSearchService.class);
		expose(IIssueAdministrationService.class);
		expose(IWorkflowService.class);
		expose(IIssueWorkService.class);
		expose(IUserManagementService.class);
		expose(IProviderAdministrationService.class);
		expose(ISearchIndexService.class);
		expose(IAuditService.class);
		// S'exposen també els conversors
		expose(StringConverter.class);
		expose(IntegerConverter.class);
		expose(BooleanConverter.class);
		expose(DateConverter.class);
		expose(IssueProjectConverter.class);
		expose(IssueStatusConverter.class);
		expose(IssueSeverityConverter.class);
		expose(IssueCategoryConverter.class);
		expose(DictionaryItemConverter.class);
		expose(SprintConverter.class);
		expose(UserConverter.class);
		
		/* Propietats de l'aplicació */
		bind(Properties.class).annotatedWith(Names.named(ApplicationConstants.NAMED_INJECTION_APPLICATION_PROPERTIES)).toInstance(applicationProperties);
		// Es necessari exposar el binding a l'exterior perquè s'ha configurat dins un mòdul privat
		expose(Properties.class).annotatedWith(Names.named(ApplicationConstants.NAMED_INJECTION_APPLICATION_PROPERTIES));
	}
	
	private Properties loadApplicationProperties()
	{
		Properties properties = new Properties();
		try{
			FileInputStream fis = new FileInputStream(new File(getClass().getResource("/org/fenrir/vespine/web/conf/application.properties").toURI()));
			properties.load(fis);
			
			fis.close();
		}		
		catch(IOException e){
			// No s'hauria de donar mai
			log.error("Error en llegir les propietats d'arrancada: " + e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		catch(URISyntaxException e){
			// No s'hauria de donar mai
			log.error("Error en llegir les propietats d'arrancada: " + e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		
		return properties;
	}
}
