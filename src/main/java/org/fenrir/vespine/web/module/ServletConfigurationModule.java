package org.fenrir.vespine.web.module;

import java.util.HashMap;

import com.google.inject.servlet.ServletModule;

import org.apache.shiro.guice.web.GuiceShiroFilter;
import org.atmosphere.guice.AtmosphereGuiceServlet;
import org.fenrir.vespine.web.RestEasyGuiceServlet;
import org.fenrir.vespine.web.filter.AccessAuditFilter;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140907
 */
public class ServletConfigurationModule extends ServletModule
{
	@SuppressWarnings("serial")
	@Override
    protected void configureServlets() 
    {
		serve("/rest/*").with(RestEasyGuiceServlet.class);
    	serve("/broadcast/*").with(AtmosphereGuiceServlet.class, 
    			new HashMap<String, String>() 
    			{
		            {
		            	// Per especificar més d'un package base utilitzar ;
		            	put("org.atmosphere.cpr.packages", "org.fenrir.vespine.web.controller.async");
		            	put("org.atmosphere.websocket.messageContentType", "application/json");
		            	put("org.atmosphere.useWebSocketAndServlet3", "false");
		                put("org.atmosphere.useNative", "true");
		            }
    			});
    	
    	/* Filtres */
    	// Totes les peticions passaran pel filtre d'autenticació
    	filter("/*").through(GuiceShiroFilter.class);
    	// session-per-http-request
    	// Les peticions a qualsevol URLs de la forma /rest/* tret de /rest/authentication/* 
    	filterRegex("/rest/(?!authentication)(.*)").through(ApplicationModule.KEY_PERSIST_FILTER_APPLICATION);
    	// Les peticions a URLs de la forma /rest/authentication/* 
    	filter("/rest/authentication/*").through(UserDBDatasourceModule.KEY_PERSIST_FILTER_USERS);
        // S'habilita l'auditoria d'accessos
    	filter("/*").through(AccessAuditFilter.class);
    }
}
