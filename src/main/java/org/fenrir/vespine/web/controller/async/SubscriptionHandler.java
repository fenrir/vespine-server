package org.fenrir.vespine.web.controller.async;

import java.util.HashMap;
import java.util.UUID;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Post;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /broadcast, es per això que no s'ha d'afegir
 * aquest prefix a la configuració del path
 * @see ManagedService
 * @author Antonio Archilla Nava
 * @version v0.1.201409013
 */
@ManagedService(path="/broadcast/subscribe",
		atmosphereConfig={"org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor.method=POST"},
		// S'elimina l'interceptor per defecte que indica la mida del missatge a la resposta
		interceptors={org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor.class, 
				org.atmosphere.interceptor.SuspendTrackerInterceptor.class
		}
)
public class SubscriptionHandler 
{
	private final Logger log = LoggerFactory.getLogger(SubscriptionHandler.class);
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
    @Post
    public void subscribe(AtmosphereResource atmosphereResource)
    {
    	Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();

		broadcastService.subscribe(username, atmosphereResource);
    }
    
    @Ready
    public String onReady(final AtmosphereResource atmosphereResource) 
    {
    	BroadcastMessage message = new BroadcastMessage();
    	message.setOriginator("system");
    	message.setType(BroadcastMessage.MESSAGE_TYPE_CONNECTION);
    	message.setExtraData(new HashMap<String, Object>(){
			{
				put(BroadcastMessage.MESSAGE_EXTRA_DATA_CONNECTION_ID, UUID.randomUUID().toString());
			}
		});
    	try{
    		System.out.println("UUID: " + atmosphereResource.uuid());
    		// Es comunica la ID al client
    		 return new ObjectMapper().writeValueAsString(message);
    	}
    	catch(Exception e){
    		log.error("Error comunicant CONNECTION ID: {}", e.getMessage(), e);
    		throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Error comunicant CONNECTION ID")
					.type(MediaType.TEXT_PLAIN).build());
    	}
    }
}
