package org.fenrir.vespine.web.controller.rest;

import java.io.File;
import java.net.URL;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO Javadoc
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140729
 */
@Path("/application")
public class ApplicationController 
{
	private final Logger log = LoggerFactory.getLogger(ApplicationController.class);
	
	@Path("/name")
	@GET
    @Produces(MediaType.TEXT_PLAIN)    
	public String getName()
	{
		log.debug("Capturada petició a /rest/application/name");
		
		return "Vespine Issue Management Server v0.1 Nymph - Yellow Jacket";
	}
	
	@Path("/release")
	@GET
    @Produces(MediaType.TEXT_PLAIN)    
	public String getRelease()
	{
		log.debug("Capturada petició a /rest/application/release");
		
		return "0.1.20140103";
	}

	@Path("/changelog")
	@GET
    @Produces(MediaType.TEXT_PLAIN)    
	public String getChangelog(@Context HttpServletResponse response)
	{
		log.debug("Capturada petició a /rest/application/changelog");
		
		try{
			URL changelogFile = getClass().getClassLoader().getResource("org/fenrir/vespine/web/changelog.txt");
			String strChangelog = FileUtils.readFileToString(new File(changelogFile.toURI()), "UTF-8");
			
			response.setStatus(HttpServletResponse.SC_OK);
			response.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN + "; charset=UTF-8");
			return strChangelog;
		}
		catch(Exception e){
			log.error("Error recuperant el registre de canvis de l'aplicació: {}", e.getMessage(), e);
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Error recuperant el registre de canvis de l'aplicació")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@Path("/credits")
	@GET
    @Produces(MediaType.TEXT_PLAIN)
	public String getCredits()
	{
		log.debug("Capturada petició a /rest/application/credits");
		
		return "Especificacions Funcionals: zbenitez" +
				"Codi: aarchilla";
	}
	
	@Path("/api.json")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getApiVersions(@Context HttpServletResponse response)
	{
		try{
			URL changelogFile = getClass().getClassLoader().getResource("org/fenrir/vespine/web/api.json");
			String strChangelog = FileUtils.readFileToString(new File(changelogFile.toURI()), "UTF-8");
			
			response.setStatus(HttpServletResponse.SC_OK);
			response.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + "; charset=UTF-8");
			return strChangelog;
		}
		catch(Exception e){
			log.error("Error recuperant l'informació de l'API: {}", e.getMessage(), e);
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Error recuperant l'informació de l'API")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
