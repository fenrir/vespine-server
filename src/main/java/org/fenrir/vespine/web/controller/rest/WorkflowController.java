package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.SerializationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.adapter.ProjectDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.WorkflowDTOAdapter;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.core.entity.WorkflowStep;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IWorkflowService;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * URL del controlador /rest/workflow
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140819
 */
@Path("/workflow")
public class WorkflowController 
{
	private final Logger log = LoggerFactory.getLogger(WorkflowController.class);	
	
	@Inject 
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IWorkflowService workflowService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setWorkflowService(IWorkflowService workflowService)
	{
		this.workflowService = workflowService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IWorkflowDTO> getWorkflows()
	{
		List<IWorkflowDTO> workflowDTOs = new ArrayList<IWorkflowDTO>();
		
		List<Workflow> workflows = workflowService.findAllWorkflows();
		for(Workflow workflow:workflows){
			workflowDTOs.add(new WorkflowDTOAdapter(workflow));
		}
		
		return workflowDTOs;
	}
	
	@GET
	@Path("/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IWorkflowDTO getWorkflowById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		Workflow workflow = workflowService.findWorkflowById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(workflow==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new WorkflowDTOAdapter(workflow);
	}
	
	@GET
	@Path("/{id}/projects.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProjectDTO> getWorkflowProjects(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		Workflow workflow = workflowService.findWorkflowById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(workflow==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		
		List<IProjectDTO> resultList = new ArrayList<IProjectDTO>();
		List<IssueProject> projects = workflowService.findProjectsByWorkflow(workflow);
		for(IssueProject project:projects){
			resultList.add(new ProjectDTOAdapter(project));
		}

		return resultList;
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response createWorkflow(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("initialStatus")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'estat inicial' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			IssueStatus initialStatus = coreAdministrationService.findStatusById(Long.valueOf(parameters.getFirst("initialStatus")));
			Map<String, IssueStatus> statusCache = new HashMap<String, IssueStatus>();
			statusCache.put(initialStatus.getId().toString(), initialStatus);
			List<WorkflowStep> stepList = new ArrayList<WorkflowStep>();
			// Deserialització dels "steps"
			List<String> jsonStepList = parameters.get("step");
			if(jsonStepList!=null){
				ObjectMapper mapper = new ObjectMapper();
				for(String elem:parameters.get("step")){
					Map<String, Object> rawStep = mapper.readValue(elem, Map.class);
					
					WorkflowStep step = new WorkflowStep();
					IssueStatus sourceStatus = null;
					String sourceStatusId = (String)((Map<String, Object>)rawStep.get("sourceStatus")).get("statusId");
					if(statusCache.containsKey(sourceStatusId)){
						sourceStatus = statusCache.get(sourceStatusId);
					}
					else{
						sourceStatus = coreAdministrationService.findStatusById(Long.valueOf(sourceStatusId));
						statusCache.put(sourceStatus.getId().toString(), sourceStatus);
					}
					step.setSourceStatus(sourceStatus);
					IssueStatus destinationStatus = null;
					String destinationStatusId = (String)((Map<String, Object>)rawStep.get("destinationStatus")).get("statusId");
					if(statusCache.containsKey(destinationStatusId)){
						destinationStatus = statusCache.get(destinationStatusId);
					}
					else{
						destinationStatus = coreAdministrationService.findStatusById(Long.valueOf(destinationStatusId));
						statusCache.put(destinationStatus.getId().toString(), destinationStatus);
					}
					step.setDestinationStatus(destinationStatus);
					Date lastUpdated = new Date((Long)rawStep.get("lastUpdated"));
					step.setLastUpdated(lastUpdated);
					stepList.add(step);
				}
			}
			final Workflow workflow = workflowService.createWorkflow(name, initialStatus, stepList);

			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_CREATE, workflow.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_WORKFLOW_CREATED, 
						"Flux de treball creat: " + workflow.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new WorkflowDTOAdapter(workflow));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació de flux de treball: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(workflow.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant fluxe de treball: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant el fluxe de treball")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response updateWorkflow(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("initialStatus")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'estat inicial' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			IssueStatus initialStatus = coreAdministrationService.findStatusById(Long.valueOf(parameters.getFirst("initialStatus")));
			Map<String, IssueStatus> statusCache = new HashMap<String, IssueStatus>();
			statusCache.put(initialStatus.getId().toString(), initialStatus);
			List<WorkflowStep> stepList = new ArrayList<WorkflowStep>();
			// Deserialització dels "steps"
			final Workflow oldWorkflow = (Workflow)SerializationUtils.clone(workflowService.findWorkflowById(id));
			List<String> jsonStepList = parameters.get("step");
			if(jsonStepList!=null){
				ObjectMapper mapper = new ObjectMapper();
				for(String elem:parameters.get("step")){
					Map<String, Object> rawStep = mapper.readValue(elem, Map.class);
					
					WorkflowStep step = new WorkflowStep();
					IssueStatus sourceStatus = null;
					String sourceStatusId = (String)((Map<String, Object>)rawStep.get("sourceStatus")).get("statusId");
					if(statusCache.containsKey(sourceStatusId)){
						sourceStatus = statusCache.get(sourceStatusId);
					}
					else{
						sourceStatus = coreAdministrationService.findStatusById(Long.valueOf(sourceStatusId));
						statusCache.put(sourceStatus.getId().toString(), sourceStatus);
					}
					step.setSourceStatus(sourceStatus);
					IssueStatus destinationStatus = null;
					String destinationStatusId = (String)((Map<String, Object>)rawStep.get("destinationStatus")).get("statusId");
					if(statusCache.containsKey(destinationStatusId)){
						destinationStatus = statusCache.get(destinationStatusId);
					}
					else{
						destinationStatus = coreAdministrationService.findStatusById(Long.valueOf(destinationStatusId));
						statusCache.put(destinationStatus.getId().toString(), destinationStatus);
					}
					step.setDestinationStatus(destinationStatus);
					// Important perquè els steps arriven al controlador sense la referència
					step.setWorkflow(oldWorkflow);
					Date lastUpdated = new Date((Long)rawStep.get("lastUpdated"));
					step.setLastUpdated(lastUpdated);
					stepList.add(step);
				}
			}
			final Workflow workflow = workflowService.updateWorkflow(id, name, initialStatus, stepList);

			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_UPDATE, workflow.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_WORKFLOW_UPDATED, 
						"Flux de treball actualitzat: " + oldWorkflow.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new WorkflowDTOAdapter(oldWorkflow));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new WorkflowDTOAdapter(workflow));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció del flux de treball: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant fluxe de treball: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_UPDATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant el fluxe de treball")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")	
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response deleteWorkflow(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator, 
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final Workflow oldWorkflow = workflowService.findWorkflowById(id);
			workflowService.deleteWorkflow(id);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_WORKFLOW_DELETED, 
						"Flux de treball eliminat: " + oldWorkflow.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new WorkflowDTOAdapter(oldWorkflow));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge d'eliminació de flux de treball: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant fluxe de treball: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant el fluxe de treball")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
