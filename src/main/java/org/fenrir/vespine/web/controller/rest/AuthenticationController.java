package org.fenrir.vespine.web.controller.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.fenrir.vespine.web.security.dao.IUserDAO;
import org.fenrir.vespine.web.security.domain.User;
import org.fenrir.vespine.web.security.dto.IUserDTO;
import org.fenrir.vespine.web.security.dto.adapter.UserDTOAdapter;

/**
 * TODO Javadoc
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140724
 */
@Path("/authentication")
public class AuthenticationController 
{
	@Inject
	private IUserDAO userDAO;
	
	public void setUserDAO(IUserDAO userDAO)
	{
		this.userDAO = userDAO;
	}
	
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public IUserDTO login(@Context HttpServletResponse response)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		
		User user = userDAO.findUserByName(username);
		if(user==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		
		response.setStatus(HttpServletResponse.SC_OK);
		return new UserDTOAdapter(user);
	}
}
