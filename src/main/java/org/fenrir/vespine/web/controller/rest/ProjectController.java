package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.SerializationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.adapter.ProjectDTOAdapter;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.ProviderProject;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.service.IProviderAdministrationService;
import org.fenrir.vespine.core.service.IWorkflowService;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * URL del controlador /rest/project
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140815
 */
@Path("/project")
public class ProjectController 
{
	private final Logger log = LoggerFactory.getLogger(ProjectController.class);	
	
	@Inject 
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueSearchService issueSearchService;
	
	@Inject
	private IWorkflowService workflowService;
	
	@Inject
	private IProviderAdministrationService providerAdministrationService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setIssueSearchService(IIssueSearchService issueSearchService)
	{
		this.issueSearchService = issueSearchService;
	}
	
	public void setWorkflowService(IWorkflowService workflowService)
	{
		this.workflowService = workflowService;
	}
	
	public void setProviderAdministrationService(IProviderAdministrationService providerAdministrationService)
	{
		this.providerAdministrationService = providerAdministrationService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProjectDTO> getProjects(@QueryParam("workflow")Long workflowId)
	{
		List<IProjectDTO> resultList = new ArrayList<IProjectDTO>();
		
		/* Cerca de projecte per workflow */
		if(workflowId!=null){
			Workflow workflow = workflowService.findWorkflowById(workflowId);
			List<IssueProject> projects = workflowService.findProjectsByWorkflow(workflow);
			for(IssueProject project:projects){
				resultList.add(new ProjectDTOAdapter(project));
			}
		}
		/* Cerca general de projectes */
		else{
			List<IssueProject> projects = coreAdministrationService.findAllProjects();
			for(IssueProject elem:projects){
				resultList.add(new ProjectDTOAdapter(elem));
			}
		}
		
		return resultList;
	}
	
	@GET
	@Path("/{projectId}/issues.count")
    @Produces(MediaType.TEXT_PLAIN)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public String countProjectIssues(@Context HttpServletResponse response, @PathParam("projectId") Long projectId)
	{
		IssueProject project = coreAdministrationService.findProjectById(projectId);
		long issueCount = issueSearchService.countIssuesByProject(project);
		return Long.toString(issueCount);
	}
	
	@GET
	@Path("/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IProjectDTO getProjectById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		IssueProject project = coreAdministrationService.findProjectById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(project==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new ProjectDTOAdapter(project);
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response createProject(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator, 
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("workflow")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'workflow' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("abbreviation")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'abbreviation' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String abbreviation = parameters.getFirst("abbreviation");
			Workflow workflow = workflowService.findWorkflowById(Long.valueOf(parameters.getFirst("workflow")));
			Set<ProviderProject> providerProjects = new HashSet<ProviderProject>();
			List<String> providerIds = parameters.get("providerProject");
			if(providerIds!=null){			
				for(String elem:parameters.get("providerProject")){
					ProviderProject providerProject = providerAdministrationService.findProviderProjectById(Long.valueOf(elem));
					providerProjects.add(providerProject);
				}
			}
			final IssueProject project = coreAdministrationService.createIssueProject(name, abbreviation, workflow, providerProjects);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_CREATE, project.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_PROJECT_CREATED, 
						"Projecte creat: " + project.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new ProjectDTOAdapter(project));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació de projecte: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(project.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant projecte: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant el projecte")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response updateProject(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("workflow")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'workflow' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			Workflow workflow = workflowService.findWorkflowById(Long.valueOf(parameters.getFirst("workflow")));
			Set<ProviderProject> providerProjects = new HashSet<ProviderProject>();
			List<String> providerIds = parameters.get("providerProject");
			if(providerIds!=null){			
				for(String elem:parameters.get("providerProject")){
					ProviderProject providerProject = providerAdministrationService.findProviderProjectById(Long.valueOf(elem));
					providerProjects.add(providerProject);
				}
			}
			final IssueProject oldProject = (IssueProject)SerializationUtils.clone(coreAdministrationService.findProjectById(id));
			final IssueProject project = coreAdministrationService.updateIssueProject(id, name, workflow, providerProjects);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_UPDATE, project.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_PROJECT_UPDATED, 
						"Projecte actualitzat: " + oldProject.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new ProjectDTOAdapter(oldProject));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new ProjectDTOAdapter(project));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció de projecte: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte modificat
			URI url = URI.create(request.getRequestURL().append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant projecte: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_UPDATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant el projecte")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")	
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response deleteProject(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final IssueProject oldProject = coreAdministrationService.findProjectById(id);
			coreAdministrationService.deleteIssueProject(id);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_PROJECT_DELETED, 
						"Projecte eliminat: " + oldProject.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new ProjectDTOAdapter(oldProject));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge d'eliminació de projecte: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant projecte: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant el projecte")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
