package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.SerializationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;
import org.fenrir.vespine.core.dto.adapter.AlertTypeDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.IssueAlertDTOAdapter;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.IssueAlert;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * URL del controlador /rest/alertType
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140822
 */
@Path("/alertType")
public class AlertTypeController 
{
	private final Logger log = LoggerFactory.getLogger(AlertTypeController.class);	
	
	@Inject 
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueSearchService issueSearchService;
	
	@Inject
	private IIssueAdministrationService issueAdministrationService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setIssueSearchService(IIssueSearchService issueSearchService)
	{
		this.issueSearchService = issueSearchService;
	}
	
	public void setIssueAdministrationService(IIssueAdministrationService issueAdministrationService)
	{
		this.issueAdministrationService = issueAdministrationService;
	}
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IAlertTypeDTO> getAlertTypes()
	{
		List<IAlertTypeDTO> resultList = new ArrayList<IAlertTypeDTO>();
		
		List<AlertType> alertTypes = coreAdministrationService.findAllAlertTypes();
		for(AlertType elem:alertTypes){
			resultList.add(new AlertTypeDTOAdapter(elem));
		}
		
		return resultList;
	}
	
	@GET
	@Path("/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IAlertTypeDTO getAlertTypeById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		AlertType alertType = coreAdministrationService.findAlertTypeById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(alertType==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new AlertTypeDTOAdapter(alertType);
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response createAlertType(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("description")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'descripció' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("alertRule")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'regla' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String description = parameters.getFirst("description");
			String icon = parameters.getFirst("icon");
			String alertRule = parameters.getFirst("alertRule");
			Boolean generateOld = Boolean.valueOf(parameters.getFirst("generateOld"));
			final AlertType alertType = coreAdministrationService.createAlertType(name, description, icon, alertRule, generateOld);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_CREATE, alertType.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ALERT_TYPE_CREATED, 
						"Tipus d'alerta creat: " + alertType.getId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new AlertTypeDTOAdapter(alertType));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació de tipus d'alerta: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(alertType.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant tipus d'alerta: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant el tipus d'alerta")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response updateAlertType(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("description")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'descripció' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("alertRule")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'regla' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String description = parameters.getFirst("description");
			String icon = parameters.getFirst("icon");
			String alertRule = parameters.getFirst("alertRule");
			final AlertType oldAlertType = (AlertType)SerializationUtils.clone(coreAdministrationService.findAlertTypeById(id));
			final AlertType alertType = coreAdministrationService.updateAlertType(id, name, description, icon, alertRule);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_UPDATE, alertType.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ALERT_TYPE_UPDATED, 
						"Tipus d'alerta actualitzat: " + oldAlertType.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new AlertTypeDTOAdapter(oldAlertType));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new AlertTypeDTOAdapter(alertType));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció de tipus d'alerta: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(alertType.getId()).append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant tipus d'alerta: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_UPDATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant el tipus d'alerta")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response deleteAlertType(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final AlertType oldAlertType = coreAdministrationService.findAlertTypeById(id);
			coreAdministrationService.deleteAlertType(id);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ALERT_TYPE_DELETED, 
						"Tipus d'alerta eliminat: " + oldAlertType.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new AlertTypeDTOAdapter(oldAlertType));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge d'eliminació de tipus d'alerta: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant tipus d'alerta: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant el tipus d'alerta")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	/* Alertes incidència */
	@GET
	@Path("/{typeId}/alerts.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IIssueAlertDTO> getIssueAlertsByType(@PathParam("typeId") Long typeId)
	{
		List<IIssueAlertDTO> resultList = new ArrayList<IIssueAlertDTO>();
		
		List<IssueAlert> alerts = issueSearchService.findAllAlertsByType(typeId);
		for(IssueAlert alert:alerts){
			resultList.add(new IssueAlertDTOAdapter(alert));
		}
		
		return resultList;
	}
	
	@POST
	@Path("/{typeId}/alerts")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response createIssueAlerts(@Context HttpServletRequest request, @PathParam("typeId") Long typeId)
	{
		try{
			AlertType alertType = coreAdministrationService.findAlertTypeById(typeId);
			issueAdministrationService.createIssueAlerts(alertType);
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error creant alertes d'incidència: {}", e.getMessage(), e);
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant alertes d'incidència")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{typeId}/alerts")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response validateIssueAlerts(@Context HttpServletRequest request, @PathParam("typeId") Long typeId)
	{
		try{
			AlertType alertType = coreAdministrationService.findAlertTypeById(typeId);
			issueAdministrationService.validateIssueAlerts(alertType);
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error validant alertes d'incidència: {}", e.getMessage(), e);
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error validant alertes d'incidència")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
