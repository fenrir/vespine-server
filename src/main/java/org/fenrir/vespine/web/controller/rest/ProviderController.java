package org.fenrir.vespine.web.controller.rest;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.fenrir.vespine.core.dto.adapter.ProviderCategoryDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ProviderProjectDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ProviderSeverityDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ProviderStatusDTOAdapter;
import org.fenrir.vespine.core.entity.ProviderCategory;
import org.fenrir.vespine.core.entity.ProviderProject;
import org.fenrir.vespine.core.entity.ProviderSeverity;
import org.fenrir.vespine.core.entity.ProviderStatus;
import org.fenrir.vespine.core.service.IProviderAdministrationService;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.core.SecurityConstants;

/**
 * TODO Javadoc
 * URL del controlador /rest/provider
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140714
 */
@Path("/provider")
public class ProviderController 
{
	@Inject
	private IProviderAdministrationService providerAdministrationService;
	
	public void setProviderAdministrationService(IProviderAdministrationService providerAdministrationService)
	{
		this.providerAdministrationService = providerAdministrationService;
	}
	
	/*---------------------------------*
     *       Providers Projectes       *
     *---------------------------------*/
	@GET
	@Path("/projects.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProviderElementDTO> getAllProviderProjects(@QueryParam("provider") String provider)
	{
		List<IProviderElementDTO> providerElementDTOs = new ArrayList<IProviderElementDTO>();
		List<ProviderProject> providerElements = providerAdministrationService.findAllProviderProjects();
		for(ProviderProject elem:providerElements){
			providerElementDTOs.add(new ProviderProjectDTOAdapter(elem));
		}
		
		return providerElementDTOs;
	}
	
	@GET
	@Path("/{provider}/projects.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProviderElementDTO> getProviderProjects(@PathParam("provider") String provider, @QueryParam("providerId") String providerId)
	{
		List<IProviderElementDTO> providerElementDTOs = new ArrayList<IProviderElementDTO>();
		List<ProviderProject> providerElements = null;
		/* Cerca per Id de provider */
		if(providerId!=null){
			ProviderProject elem = providerAdministrationService.findProviderProjectByProviderData(provider, providerId);
			providerElements = new ArrayList<ProviderProject>();
			providerElements.add(elem);
		}
		/* Cerca per provider */
		else{
			providerElements = providerAdministrationService.findProviderProjectsByProvider(provider);
		}
		
		if(providerElements!=null){
			for(ProviderProject elem:providerElements){
				providerElementDTOs.add(new ProviderProjectDTOAdapter(elem));
			}
		}
		
		return providerElementDTOs;
	}
	
	@GET
	@Path("/project/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IProviderElementDTO getProviderProjectById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		ProviderProject element = providerAdministrationService.findProviderProjectById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(element==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new ProviderProjectDTOAdapter(element);
	}
	
	/*---------------------------------*
     *         Providers Estats        *
     *---------------------------------*/
	@GET
	@Path("/status.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProviderElementDTO> getAllProviderStatus()
	{
		List<IProviderElementDTO> providerElementDTOs = new ArrayList<IProviderElementDTO>();
		List<ProviderStatus> providerElements = providerAdministrationService.findAllProviderStatus();
		for(ProviderStatus elem:providerElements){
			providerElementDTOs.add(new ProviderStatusDTOAdapter(elem));
		}
		
		return providerElementDTOs;
	}
	
	@GET
	@Path("/{provider}/status.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProviderElementDTO> getProviderStatus(@PathParam("provider") String provider, @QueryParam("providerId") String providerId)
	{
		List<IProviderElementDTO> providerElementDTOs = new ArrayList<IProviderElementDTO>();
		List<ProviderStatus> providerElements = null;
		/* Cerca per Id de provider */
		if(providerId!=null){
			ProviderStatus elem = providerAdministrationService.findProviderStatusByProviderData(provider, providerId);
			providerElements = new ArrayList<ProviderStatus>();
			providerElements.add(elem);
		}
		/* Cerca per provider */
		else{
			providerElements = providerAdministrationService.findProviderStatusByProvider(provider);
		}
		
		if(providerElements!=null){
			for(ProviderStatus elem:providerElements){
				providerElementDTOs.add(new ProviderStatusDTOAdapter(elem));
			}
		}
		
		return providerElementDTOs;
	}
	
	@GET
	@Path("/status/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IProviderElementDTO getProviderStatusById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		ProviderStatus element = providerAdministrationService.findProviderStatusById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(element==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new ProviderStatusDTOAdapter(element);
	}
	
	/*---------------------------------*
     *       Providers Severitats      *
     *---------------------------------*/
	@GET
	@Path("/severities.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProviderElementDTO> getAllProviderSeverities()
	{
		List<IProviderElementDTO> providerElementDTOs = new ArrayList<IProviderElementDTO>();
		List<ProviderSeverity> providerElements = providerAdministrationService.findAllProviderSeverities();
		for(ProviderSeverity elem:providerElements){
			providerElementDTOs.add(new ProviderSeverityDTOAdapter(elem));
		}
		
		return providerElementDTOs;
	}
	
	@GET
	@Path("/{provider}/severities.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProviderElementDTO> getProviderSeverities(@PathParam("provider") String provider, @QueryParam("providerId") String providerId)
	{
		List<IProviderElementDTO> providerElementDTOs = new ArrayList<IProviderElementDTO>();
		List<ProviderSeverity> providerElements = null;
		/* Cerca per Id de provider */
		if(providerId!=null){
			ProviderSeverity elem = providerAdministrationService.findProviderSeverityByProviderData(provider, providerId);
			providerElements = new ArrayList<ProviderSeverity>();
			providerElements.add(elem);
		}
		/* Cerca per provider */
		else{
			providerElements = providerAdministrationService.findProviderSeveritiesByProvider(provider);
		}
		
		if(providerElements!=null){
			for(ProviderSeverity elem:providerElements){
				providerElementDTOs.add(new ProviderSeverityDTOAdapter(elem));
			}
		}
		
		return providerElementDTOs;
	}
	
	@GET
	@Path("/severity/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IProviderElementDTO getProviderSeverityById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		ProviderSeverity element = providerAdministrationService.findProviderSeverityById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(element==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new ProviderSeverityDTOAdapter(element);
	}
	
	/*---------------------------------*
     *       Providers Categories      *
     *---------------------------------*/
	@GET
	@Path("/categories.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProviderElementDTO> getAllProviderCategories()
	{
		List<IProviderElementDTO> providerElementDTOs = new ArrayList<IProviderElementDTO>();
		List<ProviderCategory> providerElements = providerAdministrationService.findAllProviderCategories();
		for(ProviderCategory elem:providerElements){
			providerElementDTOs.add(new ProviderCategoryDTOAdapter(elem));
		}
		
		return providerElementDTOs;
	}
	
	@GET
	@Path("/{provider}/categories.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IProviderElementDTO> getProviderCategories(@PathParam("provider") String provider, @QueryParam("providerId") String providerId)
	{
		List<IProviderElementDTO> providerElementDTOs = new ArrayList<IProviderElementDTO>();
		List<ProviderCategory> providerElements = null;
		/* Cerca per Id de provider */
		if(providerId!=null){
			ProviderCategory elem = providerAdministrationService.findProviderCategoryByProviderData(provider, providerId);
			providerElements = new ArrayList<ProviderCategory>();
			providerElements.add(elem);
		}
		/* Cerca per provider */
		else{
			providerElements = providerAdministrationService.findProviderCategoriesByProvider(provider);
		}
		
		if(providerElements!=null){
			for(ProviderCategory elem:providerElements){
				providerElementDTOs.add(new ProviderCategoryDTOAdapter(elem));
			}
		}
		
		return providerElementDTOs;
	}
	
	@GET
	@Path("/category/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IProviderElementDTO getProviderCategoryById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		ProviderCategory element = providerAdministrationService.findProviderCategoryById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(element==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new ProviderCategoryDTOAdapter(element);
	}
}
