package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.SerializationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.adapter.CustomFieldDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.CustomValueDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.FieldTypeDTOAdapter;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueCustomValue;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * URL del controlador /rest/customField
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140828
 */
@Path("/customField")
public class CustomFieldController 
{
	private final Logger log = LoggerFactory.getLogger(CustomFieldController.class);	
	
	@Inject 
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueSearchService issueSearchService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setIssueSearchService(IIssueSearchService issueSearchService)
	{
		this.issueSearchService = issueSearchService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@GET
	@Path("/types.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IFieldType> getFieldTypes()
	{
		List<IFieldType> fieldTypes = new ArrayList<IFieldType>();
		for(FieldType type:FieldType.values()){
			fieldTypes.add(new FieldTypeDTOAdapter(type));
		}
		return fieldTypes;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<ICustomFieldDTO> getCustomFields(@QueryParam("projectId") Long projectId)
	{
		List<ICustomFieldDTO> resultList = new ArrayList<ICustomFieldDTO>();
		
		List<IssueCustomField> fields = null;
		if(projectId!=null){
			IssueProject project = coreAdministrationService.findProjectById(projectId);
			// Si no s'ha trobat s'indica el codi d'estat a la resposta
			if(project==null){
				// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
				return null;
			}
			
			fields = coreAdministrationService.findCustomFieldsByProject(project);
		}
		else{
			fields = coreAdministrationService.findAllCustomFields();
		}
		
		if(fields!=null){			
			for(IssueCustomField field:fields){
				resultList.add(new CustomFieldDTOAdapter(field));
			}
		}
		
		return resultList;
	}
	
	@GET
	@Path("/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public ICustomFieldDTO getCustomFieldById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		IssueCustomField field = coreAdministrationService.findCustomFieldById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(field==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new CustomFieldDTOAdapter(field);
	}
	
	@GET
	@Path("/{fieldId}/customValues.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<ICustomValueDTO> getCustomValues(@PathParam("fieldId") Long fieldId)
	{
		List<ICustomValueDTO> resultList = new ArrayList<ICustomValueDTO>();
		
		List<IssueCustomValue> values = issueSearchService.findCustomValuesByField(fieldId);
		if(values!=null){			
			for(IssueCustomValue value:values){
				resultList.add(new CustomValueDTOAdapter(value));
			}
		}
		
		return resultList;
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response createCustomField(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("fieldType")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'fieldType' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
				
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String strFieldType = parameters.getFirst("fieldType");
			FieldType fieldType = FieldType.valueOf(strFieldType);
			String dataProvider = parameters.getFirst("dataProvider");
			String strMandatory = parameters.getFirst("mandatory");
			Boolean mandatory = Boolean.valueOf(strMandatory);
			List<IssueProject> projectList = new ArrayList<IssueProject>();
			if(parameters.get("project")!=null){
				for(String projectId:parameters.get("project")){
					IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
					projectList.add(project);
				}
			}
			
			final IssueCustomField field = coreAdministrationService.createCustomField(name, fieldType, dataProvider, mandatory, projectList);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_CREATE, field.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_CUSTOM_FIELD_CREATED, 
						"Camp personalitzat creat: " + field.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new CustomFieldDTOAdapter(field));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació de camp personalitzat: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(field.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant camp personalitzat: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant el camp personalitzat")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response updateCustomField(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("fieldType")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'fieldType' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String strFieldType = parameters.getFirst("fieldType");
			FieldType fieldType = FieldType.valueOf(strFieldType);
			String dataProvider = parameters.getFirst("dataProvider");
			String strMandatory = parameters.getFirst("mandatory");
			Boolean mandatory = Boolean.valueOf(strMandatory);
			List<IssueProject> projectList = new ArrayList<IssueProject>();
			if(parameters.get("project")!=null){
				for(String projectId:parameters.get("project")){
					IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
					projectList.add(project);
				}
			}
			
			final IssueCustomField oldField = (IssueCustomField)SerializationUtils.clone(coreAdministrationService.findCustomFieldById(id));
			final IssueCustomField field = coreAdministrationService.updateCustomField(id, name, fieldType, dataProvider, mandatory, projectList);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_UPDATE, field.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_CUSTOM_FIELD_UPDATED, 
						"Camp personalitzat actualitzant: " + oldField.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new CustomFieldDTOAdapter(oldField));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new CustomFieldDTOAdapter(field));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció de camp personalitzat: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant camp personalitzat: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_UPDATE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant el camp personalitzat")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")	
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response deleteCustomField(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final IssueCustomField oldField = coreAdministrationService.findCustomFieldById(id);
			coreAdministrationService.deleteCustomField(id);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_CUSTOM_FIELD_DELETED, 
						"Camp personalitzat eliminat: " + oldField.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new CustomFieldDTOAdapter(oldField));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge d'eliminació de camp personalitzat: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant camp personalitzat: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant el camp personalitzat")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
