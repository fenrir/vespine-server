package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.SerializationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;
import org.fenrir.vespine.core.dto.adapter.DictionaryDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.DictionaryItemDTOAdapter;
import org.fenrir.vespine.core.entity.Dictionary;
import org.fenrir.vespine.core.entity.DictionaryItem;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * URL del controlador /rest/dictionary
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140819
 */
@Path("/dictionary")
public class DictionaryController 
{
	private final Logger log = LoggerFactory.getLogger(DictionaryController.class);	
	
	@Inject 
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IDictionaryDTO> getDictionaries()
	{
		List<IDictionaryDTO> resultList = new ArrayList<IDictionaryDTO>();
		
		List<Dictionary> dictionaries = coreAdministrationService.findAllDictionaries();
		for(Dictionary dictionary:dictionaries){
			resultList.add(new DictionaryDTOAdapter(dictionary));
		}
		
		return resultList;
	}
	
	@GET
	@Path("/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IDictionaryDTO getDictionaryById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		Dictionary dictionary = coreAdministrationService.findDictionaryById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(dictionary==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new DictionaryDTOAdapter(dictionary);
	}
	
	@GET
	@Path("/{dictionaryId}/items.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IDictionaryItemDTO> getDictionaryItems(@PathParam("dictionaryId") Long dictionaryId)
	{
		List<IDictionaryItemDTO> resultList = new ArrayList<IDictionaryItemDTO>();
		
		Dictionary dictionary = coreAdministrationService.findDictionaryById(dictionaryId);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(dictionary==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		
		List<DictionaryItem> items = dictionary.getItems();
		for(DictionaryItem item:items){
			resultList.add(new DictionaryItemDTOAdapter(item));
		}
		
		return resultList;
	}
	
	@GET
	@Path("/item/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IDictionaryItemDTO getDictionaryItemById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		DictionaryItem item = coreAdministrationService.findDictionaryItem(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(item==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new DictionaryItemDTOAdapter(item);
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response createDictionary(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("description")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'description' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String description = parameters.getFirst("description");
			List<DictionaryItem> itemList = new ArrayList<DictionaryItem>();
			List<String> jsonItemList = parameters.get("item");
			if(jsonItemList!=null){
				ObjectMapper mapper = new ObjectMapper();
				for(String elem:parameters.get("item")){
					Map<String, Object> rawStep = mapper.readValue(elem, Map.class);
					
					String itemId = (String)rawStep.get("itemId");
					String itemDescription = (String)rawStep.get("description");
					String itemValue = (String)rawStep.get("value");
					Date lastUpdated = new Date((Long)rawStep.get("lastUpdated"));
					
					DictionaryItem item = new DictionaryItem();
					item.setId(itemId!=null ? Long.valueOf(itemId) : null);
					item.setDescription(itemDescription);
					item.setValue(itemValue);
					item.setLastUpdated(lastUpdated);
					itemList.add(item);
				}
			}
			
			final Dictionary dictionary = coreAdministrationService.createDictionary(name, description, itemList);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_CREATE, dictionary.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_DICTIONARY_CREATED, 
						"Diccionari creat: " + dictionary.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new DictionaryDTOAdapter(dictionary));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació de diccionari: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(dictionary.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant diccionari: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant el diccionari")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response updateDictionary(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("description")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'description' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String description = parameters.getFirst("description");
			
			Dictionary currentDictionary = coreAdministrationService.findDictionaryById(Long.valueOf(id));
			HashMap<Long, DictionaryItem> itemCache = new HashMap<Long, DictionaryItem>();
			for(DictionaryItem elem:currentDictionary.getItems()){
				itemCache.put(elem.getId(), elem);
			}
			
			List<DictionaryItem> itemList = new ArrayList<DictionaryItem>();
			List<String> jsonItemList = parameters.get("item");
			if(jsonItemList!=null){
				ObjectMapper mapper = new ObjectMapper();
				for(String elem:parameters.get("item")){
					Map<String, Object> rawStep = mapper.readValue(elem, Map.class);
					
					String itemId = (String)rawStep.get("itemId");
					DictionaryItem item;
					if(itemId!=null){
						item = itemCache.get(Long.valueOf(itemId));
					}
					else{
						item = new DictionaryItem();
					}
					
					String itemDescription = (String)rawStep.get("description");
					String itemValue = (String)rawStep.get("value");
					Date lastUpdated = new Date((Long)rawStep.get("lastUpdated"));

					item.setDictionary(currentDictionary);
					item.setDescription(itemDescription);
					item.setValue(itemValue);
					item.setLastUpdated(lastUpdated);
					itemList.add(item);
				}
			}
			
			final Dictionary oldDictionary = (Dictionary)SerializationUtils.clone(coreAdministrationService.findDictionaryById(id));
			final Dictionary dictionary = coreAdministrationService.updateDictionary(id, name, description, itemList);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_UPDATE, dictionary.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_DICTIONARY_UPDATED, 
						"Diccionari actualitzat: " + oldDictionary.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new DictionaryDTOAdapter(oldDictionary));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new DictionaryDTOAdapter(dictionary));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció de diccionari: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant diccionari: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_UPDATE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant el diccionari")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")	
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response deleteDictionary(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final Dictionary oldDictionary = coreAdministrationService.findDictionaryById(id);
			coreAdministrationService.deleteDictionary(id);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_DICTIONARY_DELETED, 
						"Diccionari eliminat: " + oldDictionary.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new DictionaryDTOAdapter(oldDictionary));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge d'eliminació de diccionari: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant diccionari: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant el diccionari")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
