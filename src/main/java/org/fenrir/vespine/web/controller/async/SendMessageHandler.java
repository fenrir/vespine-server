package org.fenrir.vespine.web.controller.async;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.PathParam;
import org.atmosphere.config.service.Post;
import org.atmosphere.cpr.AtmosphereRequest;
import org.atmosphere.cpr.AtmosphereResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.dto.BroadcastResponse;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /broadcast, es per això que no s'ha d'afegir
 * aquest prefix a la configuració del path
 * @see ManagedService
 * @author Antonio Archilla Nava
 * @version v0.1.201409013
 */
@ManagedService(path="/broadcast/send/{receiverId}",
		// S'elimina l'interceptor per defecte que indica la mida del missatge a la resposta
		interceptors={org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor.class, 
				org.atmosphere.interceptor.SuspendTrackerInterceptor.class
		}
)
public class SendMessageHandler 
{
	private final Logger log = LoggerFactory.getLogger(SendMessageHandler.class);
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@PathParam("receiverId")
	private String receiverId;
	
	@Post
	public void sendMessage(AtmosphereResource atmosphereResource)
	{
		AtmosphereRequest request = atmosphereResource.getRequest();
		
		String sender = request.getParameter("sender");
		if(sender==null){
			sender = ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM;
		}
		String message = request.getParameter("message");
		
    	
        BroadcastResponse response = new BroadcastResponse();
        response.setFrom(sender);
        response.setMessage(message);
        
        try{
        	broadcastService.sendMessage(receiverId, response);
        }
        catch(BusinessException e){
        	throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity(e.getMessage())
					.type(MediaType.TEXT_PLAIN).build());
        }
	}
}
