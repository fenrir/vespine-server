package org.fenrir.vespine.web.controller.rest;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;
import org.fenrir.vespine.core.dto.adapter.IssueWipRegistryDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.UserDTOAdapter;
import org.fenrir.vespine.core.entity.IssueWipRegistry;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.core.service.IIssueWorkService;
import org.fenrir.vespine.core.service.IUserManagementService;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.spi.dto.IUserDTO;

/**
 * TODO Javadoc
 * URL del controlador /rest/user
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140718
 */
@Path("/user")
public class UserController 
{
	@Inject 
	private IUserManagementService userService;
	
	@Inject
	private IIssueWorkService issueWorkService;
	
	public void setUserService(IUserManagementService userService)
	{
		this.userService = userService;
	}
	
	public void setIssueWorkService(IIssueWorkService issueWorkService)
	{
		this.issueWorkService = issueWorkService;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IUserDTO> getUsers(@QueryParam("active") String active,
			@QueryParam("completeNameLike") String completeNameLike,
			@QueryParam("usernameLike") String usernameLike)
	{
		List<User> users = null;
		if(Boolean.valueOf(active)){
			users = userService.findAllActiveUsers();
		}
		else if(completeNameLike!=null){
			users = userService.findUsersCompleteNameLike(completeNameLike);
		}
		else if(usernameLike!=null){
			users = userService.findUsersUsernameLike(usernameLike);
		}
		
		List<IUserDTO> userDTOs = new ArrayList<IUserDTO>();
		if(users!=null){
			for(User user:users){
				userDTOs.add(new UserDTOAdapter(user));
			}
		}
		return userDTOs;
	}
	
	@GET
	@Path("/{username}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IUserDTO getUserByUsername(@Context HttpServletResponse response, @PathParam("username") String username)
	{
		User user = userService.findUserByUsername(username);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(user==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new UserDTOAdapter(user);
	}
	
	/* Registres WIP */
	@GET
	@Path("/{user}/wipRegistries.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IIssueWipRegistryDTO> findIssueWorkInProgressRegistries(@PathParam("user") String username)
	{
		List<IIssueWipRegistryDTO> resultList = new ArrayList<IIssueWipRegistryDTO>();
		
		User user = userService.findUserByUsername(username);
		List<IssueWipRegistry> registries = issueWorkService.findIssueWorkInProgressRegistriesByUser(user);
		for(IssueWipRegistry registry:registries){
			resultList.add(new IssueWipRegistryDTOAdapter(registry));
		}
		return resultList;
	}
}
