package org.fenrir.vespine.web.controller.rest;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ISearchIndexService;
import org.fenrir.vespine.core.SecurityConstants;

/**
 * TODO Javadoc
 * URL del controlador /rest/index
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140710
 */
@Path("/index")
public class IndexController 
{
private final Logger log = LoggerFactory.getLogger(IndexController.class);	
	
	@Inject
	private ISearchIndexService indexService;
	
	@Inject
	private IAuditService auditService;
	
	public void setIndexService(ISearchIndexService indexService)
	{
		this.indexService = indexService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	@POST
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_SYSTEM)
	public Response createIndex()
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			indexService.createIndex();
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEARCH_INDEX_CREATE, null, username, AuditConstants.RESULT_CODE_OK, null);
			
			return Response.status(Response.Status.CREATED).build();
		}
		catch(Exception e){
			log.error("Error creant categoria: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEARCH_INDEX_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant l'índex")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_SYSTEM)
	public Response setIndexEnabled(MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("enable")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'enable' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		String strEnable = parameters.getFirst("enable");
		if(Boolean.TRUE.toString().equals(strEnable) || Boolean.FALSE.toString().equals(strEnable)){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("L'opció indicada no és vàlida")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		String auditAction = null;
		try{
			if(Boolean.valueOf(strEnable)){
				auditAction = AuditConstants.ACTION_SEARCH_INDEX_ENABLE;
				indexService.enableIndex();
			}
			else{
				auditAction = AuditConstants.ACTION_SEARCH_INDEX_DISABLE;
				indexService.disableIndex();
			}
			
			// Auditoria
			auditService.auditAction(auditAction, null, username, AuditConstants.RESULT_CODE_OK, null);
			
			return Response.status(Response.Status.CREATED).build();
		}
		catch(Exception e){
			log.error("Error creant categoria: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(auditAction, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error (des)habilitant l'índex")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
