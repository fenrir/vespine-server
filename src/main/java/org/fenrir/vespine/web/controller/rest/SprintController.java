package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.SerializationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.adapter.SprintDTOAdapter;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueWorkService;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * URL del controlador /rest/sprint
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140822
 */
@Path("/sprint")
public class SprintController 
{
	private final Logger log = LoggerFactory.getLogger(SprintController.class);	
	
	@Inject 
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueWorkService issueWorkService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setIssueWorkService(IIssueWorkService issueWorkService)
	{
		this.issueWorkService = issueWorkService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<ISprintDTO> getSprints(@QueryParam("projectName") String projectName,
			@QueryParam("startDatePart") String startDatePart,
			@QueryParam("endDatePart") String endDatePart)
	{
		List<ISprintDTO> resultList = new ArrayList<ISprintDTO>();
		
		List<Sprint> sprints = null;
		/* Cerca per nom de projecte */
		if(projectName!=null){
			sprints = issueWorkService.findSprintsByProjectNameLike(projectName);
		}
		/* Cerca per data inici */
		if(startDatePart!=null){
			sprints = issueWorkService.findSprintsByStartDateLike(startDatePart);
		}
		/* Cerca per data final */
		if(startDatePart!=null){
			sprints = issueWorkService.findSprintsByEndDateLike(endDatePart);
		}
		/* Cerca general */
		else{
			sprints = issueWorkService.findAllSprints();
		}
		
		if(sprints!=null){
			for(Sprint sprint:sprints){
				resultList.add(new SprintDTOAdapter(sprint));
			}
		}
		
		return resultList;
	}
	
	@GET
	@Path("/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public ISprintDTO getSprintById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		Sprint sprint = issueWorkService.findSprintById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(sprint==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new SprintDTOAdapter(sprint);
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response createSprint(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("startDate")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'startDate' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("endDate")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'endDate' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String strStartDate = parameters.getFirst("startDate");
			String strEndDate = parameters.getFirst("endDate");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String projectId = parameters.getFirst("project");
			IssueProject project = null;
			if(projectId!=null){
				project = coreAdministrationService.findProjectById(Long.parseLong(projectId));
			}
			final Sprint sprint = issueWorkService.createSprint(project, dateFormat.parse(strStartDate), dateFormat.parse(strEndDate));
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_CREATE, sprint.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_SPRINT_CREATED, 
						"Sprint creat: " + sprint.toString(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new SprintDTOAdapter(sprint));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació d'sprint: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(sprint.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant sprint: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant l'sprint")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response updateSprint(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("startDate")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'startDate' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("endDate")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'endDate' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String strStartDate = parameters.getFirst("startDate");
			String strEndDate = parameters.getFirst("endDate");
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String projectId = parameters.getFirst("project");
			IssueProject project = coreAdministrationService.findProjectById(Long.parseLong(projectId));
			
			final Sprint oldSprint = (Sprint)SerializationUtils.clone(issueWorkService.findSprintById(id));
			final Sprint sprint = issueWorkService.updateSprint(id, project, dateFormat.parse(strStartDate), dateFormat.parse(strEndDate));
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_UPDATE, sprint.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_SPRINT_UPDATED, 
						"Sprint actualitzat: " + oldSprint.toString(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new SprintDTOAdapter(oldSprint));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new SprintDTOAdapter(sprint));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció d'sprint: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant sprint: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_UPDATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant l'sprint")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response deleteSprint(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final Sprint oldSprint = issueWorkService.findSprintById(id);
			issueWorkService.deleteSprint(id);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_SPRINT_DELETED, 
						"Sprint eliminat: " + oldSprint.toString(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new SprintDTOAdapter(oldSprint));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge d'eliminació d'sprint: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant sprint: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant l'sprint")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
