package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.SerializationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.adapter.IssueDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ViewFilterDTOAdapter;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueViewFilter;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;
import org.fenrir.vespine.core.SecurityConstants;

/**
 * TODO Javadoc
 * URL del controlador /rest/viewFilter
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140822
 */
@Path("/viewFilter")
public class ViewFilterController 
{
	private final Logger log = LoggerFactory.getLogger(ViewFilterController.class);	
	
	@Inject 
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueSearchService issueSearchService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setIssueSearchService(IIssueSearchService issueSearchService)
	{
		this.issueSearchService = issueSearchService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IViewFilterDTO> getFilters(@QueryParam("applicationOnly")Boolean applicationOnly,
			@QueryParam("user")String username)
	{
		List<IViewFilterDTO> resultList = new ArrayList<IViewFilterDTO>();
		
		List<IssueViewFilter> filterList = null;
		/* Cerca de filtres d'aplicació */
		if(applicationOnly!=null && applicationOnly){
			filterList = coreAdministrationService.findApplicationViewFilters();
		}
		/* Cerca per usuari */
		else{
			filterList = coreAdministrationService.findUserViewFilters();
		}
		
		if(filterList!=null){
			for(IssueViewFilter filter:filterList){
				resultList.add(new ViewFilterDTOAdapter(filter));
			}
		}
		
		return resultList;
	}
	
	@GET
	@Path("/issues.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IIssueDTO> getFilteredIssues(@QueryParam("query")String query,
			@QueryParam("orderByClause") String orderByClause, 
			@QueryParam("page") Integer page,
			@QueryParam("pageSize") Integer pageSize)
	{
		List<IIssueDTO> resultList = new ArrayList<IIssueDTO>();
		
		if(page==null){
			page = 0;
		}
		if(pageSize==0){
			pageSize = 10;
		}
		
		List<AbstractIssue> issues = issueSearchService.findFilteredIssues(query, orderByClause, page, pageSize);
		
		for(AbstractIssue issue:issues){
			resultList.add(new IssueDTOAdapter(issue));
		}
		
		return resultList;
	}
	
	@GET
	@Path("/issues.count")
    @Produces(MediaType.TEXT_PLAIN)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public String countFilteredIssues(@Context HttpServletResponse response, 
			@QueryParam("query")String query,
			@QueryParam("issue")Long issueId)
	{
		if(issueId!=null){
			return issueSearchService.issueFilterMatches(query, issueId) ? "1" : "0";
		}
		
		long issueCount = issueSearchService.countFilteredIssues(query);
		return Long.toString(issueCount);
	}
	
	@GET
	@Path("/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IViewFilterDTO getFilterById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		IssueViewFilter filter = coreAdministrationService.findViewFilterById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(filter==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new ViewFilterDTOAdapter(filter);
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response createFilter(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("description")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'description' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("query")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'query' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String description = parameters.getFirst("description");
			String query = parameters.getFirst("query");
			String orderByClause = parameters.getFirst("orderByClause");
			
			final IssueViewFilter filter = coreAdministrationService.createIssueViewFilter(name, description, query, orderByClause);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_CREATE, filter.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_CREATED, 
						"Filtre creat: " + filter.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new ViewFilterDTOAdapter(filter));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació de filtre: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(filter.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant filtre: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant el filtre")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response updateFilter(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id,
			MultivaluedMap<String, String> parameters)
	{
		// Operació de reordenat
		boolean reorderOperation = parameters.containsKey("order");
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		/* Update de les dades del filtre */
		if(!reorderOperation){
			if(!parameters.containsKey("name")){
				throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
						.entity("El camp 'nom' és obligatori")
						.type(MediaType.TEXT_PLAIN).build());
			}
			if(!parameters.containsKey("description")){
				throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
						.entity("El camp 'description' és obligatori")
						.type(MediaType.TEXT_PLAIN).build());
			}
			if(!parameters.containsKey("query")){
				throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
						.entity("El camp 'query' és obligatori")
						.type(MediaType.TEXT_PLAIN).build());
			}
			
			try{
				/* Es recuperen els paràmetres i es crida a la capa de negoci per
				 * que faci tota la feina 
				 */
				String name = parameters.getFirst("name");
				String description = parameters.getFirst("description");
				String query = parameters.getFirst("query");
				String orderByClause = parameters.getFirst("orderByClause");
				
				final IssueViewFilter oldFilter = (IssueViewFilter)SerializationUtils.clone(coreAdministrationService.findViewFilterById(id));
				final IssueViewFilter filter = coreAdministrationService.updateIssueViewFilter(id, name, description, query, orderByClause);
					
				// Auditoria
				auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_UPDATE, filter.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
				
				try{
					broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
							originator,
							BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_UPDATED, 
							"Filtre actualitzat: " + oldFilter.getName(), 
							new HashMap<String, Object>()
							{
								{
									put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new ViewFilterDTOAdapter(oldFilter));
									put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new ViewFilterDTOAdapter(filter));
								}
							}
					);
				}
				catch(Exception e){
					log.error("Error notificant missatge de modificanció de filtre: {}", e.getMessage(), e);
				}
				
				// Format de la resposta amb la url del projecte creat
				URI url = URI.create(request.getRequestURL().append(".json").toString());
				return Response.status(Response.Status.OK).location(url).build();
			}
			catch(Exception e){
				log.error("Error modificant filtre: {}", e.getMessage(), e);
				
				// Auditoria
				auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_UPDATE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
				
				throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity("S'ha produit un error modificant el filtre")
						.type(MediaType.TEXT_PLAIN).build());
			}
		}
		/* Reordenació del filtre */
		else{
			try{
				int order = Integer.valueOf(parameters.getFirst("order"));
				
				coreAdministrationService.reorderIssueViewFilter(id, order);
				
				// Auditoria
				auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_REORDER, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
				
				try{
					broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
							originator,
							BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_UPDATED, 
							"Filtre reordenat", 
							null
					);
				}
				catch(Exception e){
					log.error("Error notificant missatge de reordenació de filtre: {}", e.getMessage(), e);
				}
				
				// Format de la resposta amb la url del projecte creat
				URI url = URI.create(request.getRequestURL().append(".json").toString());
				return Response.status(Response.Status.OK).location(url).build();
			}
			catch(Exception e){
				log.error("Error reordenant filtre: {}", e.getMessage(), e);
				
				// Auditoria
				auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_REORDER, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
				
				throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity("S'ha produit un error reordenant el filtre")
						.type(MediaType.TEXT_PLAIN).build());
			}
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response deleteFilter(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator, 
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final IssueViewFilter oldFilter = coreAdministrationService.findViewFilterById(id);
			coreAdministrationService.deleteIssueViewFilter(id);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_DELETED, 
						"Filtre eliminat: " + oldFilter.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new ViewFilterDTOAdapter(oldFilter));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge d'eliminació de filtre: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant filtre: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant el filtre")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
