package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.SerializationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.adapter.StatusDTOAdapter;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.ProviderStatus;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.service.IProviderAdministrationService;
import org.fenrir.vespine.core.service.IWorkflowService;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * URL del controlador /rest/status
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140819
 */
@Path("/status")
public class StatusController 
{
	private final Logger log = LoggerFactory.getLogger(StatusController.class);	
	
	@Inject 
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueSearchService issueSearchService;
	
	@Inject
	private IWorkflowService workflowService;
	
	@Inject
	private IProviderAdministrationService providerAdministrationService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setIssueSearchService(IIssueSearchService issueSearchService)
	{
		this.issueSearchService = issueSearchService;
	}
	
	public void setWorkflowService(IWorkflowService workflowService)
	{
		this.workflowService = workflowService;
	}
	
	public void setProviderAdministrationService(IProviderAdministrationService providerAdministrationService)
	{
		this.providerAdministrationService = providerAdministrationService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IStatusDTO> getStatusList(@QueryParam("projectId")Long projectId, 
			@QueryParam("currentStatus") Long currentStatus)
	{
		List<IStatusDTO> resultList = new ArrayList<IStatusDTO>();
		
		List<IssueStatus> statusList = coreAdministrationService.findAllStatus();
		for(IssueStatus elem:statusList){
			resultList.add(new StatusDTOAdapter(elem));
		}
		
		return resultList;
	}
	
	@GET
	@Path("/next.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IStatusDTO> getNextStatus(@QueryParam("projectId")Long projectId, 
			@QueryParam("currentStatus") Long currentStatus)
	{
		List<IStatusDTO> resultList = new ArrayList<IStatusDTO>();
		
		IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
		IssueStatus objCurrentStatus = currentStatus!=null ? coreAdministrationService.findStatusById(currentStatus) : null;
		List<IssueStatus> statusList = workflowService.findNextWorkflowStatus(project, objCurrentStatus);
		for(IssueStatus elem:statusList){
			resultList.add(new StatusDTOAdapter(elem));
		}
		
		return resultList;
	}
	
	@GET
	@Path("/{statusId}/issues.count")
    @Produces(MediaType.TEXT_PLAIN)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public String countStatusIssues(@Context HttpServletResponse response, @PathParam("statusId") Long statusId)
	{
		IssueStatus status = coreAdministrationService.findStatusById(statusId);
		long issueCount = issueSearchService.countIssuesByStatus(status);
		return Long.toString(issueCount);
	}
	
	@GET
	@Path("/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IStatusDTO getStatusById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		IssueStatus status = coreAdministrationService.findStatusById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(status==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new StatusDTOAdapter(status);
	}
	
	@GET
	@Path("/{id}/color")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public String getStatusColor(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		IssueStatus status = coreAdministrationService.findStatusById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(status==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return status.getColor();
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response createStatus(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("color")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'color' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String color = parameters.getFirst("color");
			Set<ProviderStatus> providerStatusList = new HashSet<ProviderStatus>();
			List<String> providerIds = parameters.get("providerStatus");
			if(providerIds!=null){			
				for(String elem:parameters.get("providerStatus")){
					ProviderStatus providerStatus = providerAdministrationService.findProviderStatusById(Long.valueOf(elem));
					providerStatusList.add(providerStatus);
				}
			}
			final IssueStatus status = coreAdministrationService.createIssueStatus(name, color, providerStatusList);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_CREATE, status.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_STATUS_CREATED, 
						"Estat creat: " + status.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new StatusDTOAdapter(status));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació d'estat: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del estat creat
			URI url = URI.create(request.getRequestURL().append("/").append(status.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant estat: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant l'estat")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response updateStatus(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("color")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'color' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String color = parameters.getFirst("color");
			Set<ProviderStatus> providerStatusList = new HashSet<ProviderStatus>();
			List<String> providerIds = parameters.get("providerStatus");
			if(providerIds!=null){			
				for(String elem:parameters.get("providerStatus")){
					ProviderStatus providerStatus = providerAdministrationService.findProviderStatusById(Long.valueOf(elem));
					providerStatusList.add(providerStatus);
				}
			}
			final IssueStatus oldStatus = (IssueStatus)SerializationUtils.clone(coreAdministrationService.findStatusById(id));
			final IssueStatus status = coreAdministrationService.updateIssueStatus(id, name, color, providerStatusList);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_UPDATE, status.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_STATUS_UPDATED, 
						"Estat actualitzat: " + oldStatus.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new StatusDTOAdapter(oldStatus));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new StatusDTOAdapter(status));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció d'estat: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del estat creat
			URI url = URI.create(request.getRequestURL().append("/").append(status.getId()).append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant estat: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_UPDATE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant l'estat")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")	
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response deleteStatus(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator, 
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final IssueStatus oldStatus = coreAdministrationService.findStatusById(id);
			coreAdministrationService.deleteIssueStatus(id);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_STATUS_DELETED, 
						"Estat eliminat: " + oldStatus.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new StatusDTOAdapter(oldStatus));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge d'eliminació d'estat: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant estat: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant l'estat")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
