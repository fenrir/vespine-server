package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.service.IIssueWorkService;
import org.fenrir.vespine.core.service.IUserManagementService;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;
import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.core.dto.adapter.CustomValueDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.IssueAlertDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.IssueDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.IssueNoteDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.IssueWipRegistryDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.TagDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.WorkRegistryDTOAdapter;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueAlert;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueCustomValue;
import org.fenrir.vespine.core.entity.IssueNote;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.IssueWipRegistry;
import org.fenrir.vespine.core.entity.IssueWorkRegistry;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.entity.Tag;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;

/**
 * TODO Javadoc
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140825
 */
@Path("/issue")
public class IssueController 
{
	private final Logger log = LoggerFactory.getLogger(IssueController.class);

	@Inject
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueSearchService issueSearchService;
	
	@Inject
	private IIssueAdministrationService issueAdministrationService;
	
	@Inject
	private IIssueWorkService issueWorkService;
	
	@Inject
	private IUserManagementService userManagementService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setIssueSearchService(IIssueSearchService issueSearchService)
	{
		this.issueSearchService = issueSearchService;
	}
	
	public void setIssueAdministrationService(IIssueAdministrationService issueAdministrationService)
	{
		this.issueAdministrationService = issueAdministrationService;
	}
	
	public void setIssueWorkService(IIssueWorkService issueWorkService)
	{
		this.issueWorkService = issueWorkService;
	}
	
	public void setUserManagementService(IUserManagementService userManagementService)
	{
		this.userManagementService = userManagementService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	/*---------------------------------*
     *           Incidències           *
     *---------------------------------*/
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IIssueDTO> getIssues(@QueryParam("tag")Long tagId,
			@QueryParam("visibleId") String visibleId,
			@QueryParam("query") String query,
			@QueryParam("page") Integer page, 
			@QueryParam("pageSize") Integer pageSize)
	{
		List<IIssueDTO> issueDTOs = new ArrayList<IIssueDTO>();
		
		if(page==null){
			page = 0;
		}
		if(pageSize==null){
			pageSize = 10;
		}
		
		Collection<AbstractIssue> issues = null;
		if(query!=null){
			SearchQuery objQuery = new SearchQuery();
			objQuery.setIndexQuery(query);
			issues = issueSearchService.findIssueSearchHits(objQuery, page, pageSize);
		}
		else if(visibleId!=null){
			AbstractIssue issue = issueSearchService.findIssueByVisibleId(visibleId);
			if(tagId!=null){
				Set<Tag> tags = issue.getTags();
				Iterator<Tag> iterator = tags.iterator();
				boolean found = false;
				while(iterator.hasNext() && !found){
					Tag tag = iterator.next();
					if(tag.getId().equals(tagId)){
						found = true;
						issues = new ArrayList<AbstractIssue>();
						issues.add(issue);
					}
				}
			}
			else{
				issues = new ArrayList<AbstractIssue>();
				issues.add(issue);
			}
		}
		else if(tagId!=null){
			issues = issueSearchService.findTaggedIssues(tagId, page, pageSize);
		}
		else{
			issueSearchService.findAllIssues(page, pageSize);
		}

		if(issues!=null){
			for(AbstractIssue issue:issues){
				issueDTOs.add(new IssueDTOAdapter(issue));
			}
		}
		return issueDTOs;
	}
	
	@GET
	@Path("/search.count")
    @Produces(MediaType.TEXT_PLAIN)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public String countIssues(@Context HttpServletResponse response, @QueryParam("query") String query)
	{
		SearchQuery objQuery = new SearchQuery(); 
		objQuery.setIndexQuery(query);
		long issueCount = issueSearchService.countIssueSearchHits(objQuery);
		return Long.toString(issueCount);
	}
	
	@GET
	@Path("/{issueId}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IIssueDTO getIssueById(@Context HttpServletResponse response, 
			@PathParam("issueId") Long issueId)
	{
		AbstractIssue issue = issueSearchService.findIssueById(issueId);
		if(issue!=null){
			// S'indica a la resposta que s'ha trobat
			response.setStatus(HttpServletResponse.SC_OK);
			return new IssueDTOAdapter(issue);
		}
		
		// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
		return null;
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response createIssue(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("project")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'project' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("category")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'category' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("severity")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'severity' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("status")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'status' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("summary")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'summary' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("description")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'description' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("assignedUsername")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'assignedUsername' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		Long auditRegistryReference = auditService.auditAction(AuditConstants.ACTION_ISSUE_CREATE, username);
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(parameters.getFirst("project")));
			IssueCategory category = coreAdministrationService.findCategoryById(Long.valueOf(parameters.getFirst("category")));
			IssueSeverity severity = coreAdministrationService.findSeverityById(Long.valueOf(parameters.getFirst("severity")));
			IssueStatus status = coreAdministrationService.findStatusById(Long.valueOf(parameters.getFirst("status")));
			String summary = parameters.getFirst("summary");
			String description = parameters.getFirst("description");
			User reporterUser = userManagementService.findUserByUsername(username);
			User assignedUser = null;
			if(parameters.containsKey("assignedUsername")){
				assignedUser = userManagementService.findUserByUsername(parameters.getFirst("assignedUsername"));
			}
			Sprint sprint = null;
			if(parameters.containsKey("sprint")){
				sprint = issueWorkService.findSprintById(Long.valueOf(parameters.getFirst("sprint")));
			}
			Integer estimatedTime = null;
			if(parameters.containsKey("estimatedTime")){
				estimatedTime = Integer.valueOf(parameters.getFirst("estimatedTime"));
			}
			Date slaDate = null;
			if(parameters.containsKey("slaDate")){
				slaDate = new SimpleDateFormat("dd/MM/yyyy").parse(parameters.getFirst("slaDate"));
			}
			Map<IssueCustomField, String> customFields = new HashMap<IssueCustomField, String>();
			if(parameters.containsKey("customField")){
				for(String strField:parameters.get("customField")){
					String fieldId = StringUtils.substringBefore(strField, ":");
					IssueCustomField field = coreAdministrationService.findCustomFieldById(Long.valueOf(fieldId));
					String value = StringUtils.substringAfter(strField, ":");
					customFields.put(field, value);
				}
			}
			
			final AbstractIssue issue = issueAdministrationService.createIssue(project, category, severity, status, summary, description, reporterUser, assignedUser, sprint, estimatedTime, slaDate, customFields, auditRegistryReference);
				
			// Auditoria
	        auditService.resolveAuditAction(auditRegistryReference, issue.getId().toString(), AuditConstants.RESULT_CODE_OK, null);
	        
	        try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ISSUE_CREATED, 
						"Incidència creada: " + issue.getVisibleId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new IssueDTOAdapter(issue));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació de projecte: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url de l'element creat
			URI url = URI.create(request.getRequestURL().append("/").append(issue.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant incidència: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.resolveAuditAction(auditRegistryReference, null, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant l'incidència")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response updateIssue(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long issueId,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("project")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'project' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("category")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'category' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("severity")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'severity' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("status")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'status' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("summary")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'summary' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("description")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'description' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("assignedUsername")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'assignedUsername' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		Long auditRegistryReference = auditService.auditAction(AuditConstants.ACTION_ISSUE_UPDATE, username);
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(parameters.getFirst("project")));
			IssueCategory category = coreAdministrationService.findCategoryById(Long.valueOf(parameters.getFirst("category")));
			IssueSeverity severity = coreAdministrationService.findSeverityById(Long.valueOf(parameters.getFirst("severity")));
			IssueStatus status = coreAdministrationService.findStatusById(Long.valueOf(parameters.getFirst("status")));
			String summary = parameters.getFirst("summary");
			String description = parameters.getFirst("description");
			User assignedUser = null;
			if(parameters.containsKey("assignedUsername")){
				assignedUser = userManagementService.findUserByUsername(parameters.getFirst("assignedUsername"));
			}
			Sprint sprint = null;
			if(parameters.containsKey("sprint")){
				sprint = issueWorkService.findSprintById(Long.valueOf(parameters.getFirst("sprint")));
			}
			Integer estimatedTime = null;
			if(parameters.containsKey("estimatedTime")){
				estimatedTime = Integer.valueOf(parameters.getFirst("estimatedTime"));
			}
			Date slaDate = null;
			if(parameters.containsKey("slaDate")){
				slaDate = new SimpleDateFormat("dd/MM/yyyy").parse(parameters.getFirst("slaDate"));
			}
			Date resolutionDate = null;
			if(parameters.containsKey("resolutionDate")){
				resolutionDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(parameters.getFirst("resolutionDate"));
			}
			Map<IssueCustomField, String> customFields = new HashMap<IssueCustomField, String>();
			if(parameters.containsKey("customField")){
				for(String strField:parameters.get("customField")){
					String fieldId = StringUtils.substringBefore(strField, ":");
					IssueCustomField field = coreAdministrationService.findCustomFieldById(Long.valueOf(fieldId));
					String value = StringUtils.substringAfter(strField, ":");
					customFields.put(field, value);
				}
			}
			
			final AbstractIssue oldIssue = (AbstractIssue)SerializationUtils.clone(issueSearchService.findIssueById(issueId));
			final AbstractIssue issue = issueAdministrationService.updateIssue(issueId, project, category, severity, status, summary, description, assignedUser, sprint, estimatedTime, slaDate, resolutionDate, customFields, auditRegistryReference);
				
			// Auditoria
	        auditService.resolveAuditAction(auditRegistryReference, issueId.toString(), AuditConstants.RESULT_CODE_OK, null);
	        
	        try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ISSUE_UPDATED, 
						"Incidència actualitzada: " + oldIssue.getVisibleId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new IssueDTOAdapter(oldIssue));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new IssueDTOAdapter(issue));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció d'incidència: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url de l'element creat
			URI url = URI.create(request.getRequestURL().append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant incidència: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.resolveAuditAction(auditRegistryReference, issueId.toString(), AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant l'incidència")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	/**
	 * TODO Missatge broadcast ISSUE_UPDATED
	 */
	@PUT
	@Path("/{issueId}/restore")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response restoreIssue(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("issueId") Long issueId)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			issueAdministrationService.restoreIssueFromRecycleBin(issueId);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_RESTORE, issueId.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error modificant incidència: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_RESTORE, issueId.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant incidència")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{issueId}/sla")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response updateIssueSLADate(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("issueId") Long issueId,
			@FormParam("isSla") Boolean isSla,
			@FormParam("slaDate") String strSlaDate)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			if(isSla!=null){
				isSla = strSlaDate!=null;
			}
			
			final AbstractIssue oldIssue = (AbstractIssue)SerializationUtils.clone(issueSearchService.findIssueById(issueId));
			if(strSlaDate==null){
				issueAdministrationService.setIssueSLADate(issueId, isSla);
			}
			else{
				issueAdministrationService.setIssueSLADate(issueId, new SimpleDateFormat("dd/MM/yyyy").parse(strSlaDate));
			}
			final AbstractIssue issue = issueSearchService.findIssueById(issueId);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_UPDATE_SLA_DATE, issueId.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ISSUE_UPDATED, 
						"Incidència actualitzada: " + oldIssue.getVisibleId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new IssueDTOAdapter(oldIssue));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new IssueDTOAdapter(issue));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció d'incidència: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error modificant incidència: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_UPDATE_SLA_DATE, issueId.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant incidència")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response deleteIssue(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		
		String action = null;
		AbstractIssue issue = issueSearchService.findIssueById(id);
		if(issue.isDeleted()){
			action = AuditConstants.ACTION_ISSUE_DELETE;
		}
		else{
			action = AuditConstants.ACTION_ISSUE_SEND2TRASH_BIN;
		}
		try{
			final AbstractIssue oldIssue = (AbstractIssue)SerializationUtils.clone(issueSearchService.findIssueById(id));
			issueAdministrationService.deleteIssueRecycleBinAware(id);

			// Auditoria
			auditService.auditAction(action, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ISSUE_DELETED, 
						"Incidència eliminada: " + oldIssue.getVisibleId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new IssueDTOAdapter(oldIssue));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge d'eliminació d'incidència: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant incidència: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(action, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant incidència")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	/*---------------------------------*
     *              Notes              *
     *---------------------------------*/
	@GET
	@Path("/{issueId}/notes.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IIssueNoteDTO> getIssueNotes(@PathParam("issueId") Long issueId)
	{
		List<IIssueNoteDTO> resultList = new ArrayList<IIssueNoteDTO>();
		
		AbstractIssue issue = issueSearchService.findIssueById(issueId);
		if(issue.getNotes()!=null){			
			for(IssueNote note:issue.getNotes()){
				resultList.add(new IssueNoteDTOAdapter(note));
			}
		}
		
		return resultList;
	}
	
	@GET
	@Path("/{issueId}/note/{noteId}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IIssueNoteDTO getIssueNoteById(@Context HttpServletResponse response, 
			@PathParam("issueId") Long issueId,
			@PathParam("noteId") Long noteId)
	{
		AbstractIssue issue = issueSearchService.findIssueById(issueId);
		Iterator<IssueNote> iterator = issue.getNotes().iterator();
		while(iterator.hasNext()){
			IssueNote note = iterator.next();
			if(note.getId().equals(noteId)){
				// S'indica a la resposta que s'ha trobat
				response.setStatus(HttpServletResponse.SC_OK);
				return new IssueNoteDTOAdapter(note);
			}
		}
		
		// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
		return null;
	}
	
	@POST
	@Path("/{issueId}/note")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response createNote(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("issueId") Long issueId,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("noteContents")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'noteContents' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String noteContents = parameters.getFirst("noteContents");
			
			final AbstractIssue oldIssue = (AbstractIssue)SerializationUtils.clone(issueSearchService.findIssueById(issueId));
			User user = userManagementService.findUserByUsername(username);
			IssueNote note = issueAdministrationService.createIssueNote(oldIssue, user, noteContents);
			final AbstractIssue issue = issueSearchService.findIssueById(issueId);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_CREATE, note.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ISSUE_UPDATED, 
						"Incidència actualitzada: " + oldIssue.getVisibleId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new IssueDTOAdapter(oldIssue));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new IssueDTOAdapter(issue));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció d'incidència: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(note.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant nota: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant nota")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{issueId}/note/{noteId}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response updateNote(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("issueId") Long issueId,
			@PathParam("noteId") Long noteId,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("noteContents")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'noteContents' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String noteContents = parameters.getFirst("noteContents");
			
			final AbstractIssue oldIssue = (AbstractIssue)SerializationUtils.clone(issueSearchService.findIssueById(issueId));
			IssueNote note = issueAdministrationService.updateIssueNote(noteId, noteContents);
			final AbstractIssue issue = issueSearchService.findIssueById(issueId);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_UPDATE, note.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ISSUE_UPDATED, 
						"Incidència actualitzada: " + oldIssue.getVisibleId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new IssueDTOAdapter(oldIssue));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new IssueDTOAdapter(issue));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció d'incidència: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant nota: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_UPDATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant nota")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{issueId}/note/{noteId}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response deleteNote(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("issueId") Long issueId, 
			@PathParam("noteId") Long noteId)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final AbstractIssue oldIssue = (AbstractIssue)SerializationUtils.clone(issueSearchService.findIssueById(issueId));
			issueAdministrationService.deleteIssueNote(noteId);
			final AbstractIssue issue = issueSearchService.findIssueById(issueId);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_DELETE, issueId.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ISSUE_UPDATED, 
						"Incidència actualitzada: " + oldIssue.getVisibleId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new IssueDTOAdapter(oldIssue));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new IssueDTOAdapter(issue));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció d'incidència: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant nota: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_DELETE, issueId.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant la nota")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	/*---------------------------------*
     *              Tags               *
     *---------------------------------*/
	@GET
	@Path("/{issueId}/tags.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<ITagDTO> getIssueTags(@PathParam("issueId") Long issueId)
	{
		List<ITagDTO> resultList = new ArrayList<ITagDTO>();
		
		AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(issueId));
		for(Tag tag:issue.getTags()){
			resultList.add(new TagDTOAdapter(tag));
		}
		
		return resultList;
	}
	
	@POST
	@Path("/{issueId}/tag")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response tagIssue(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("issueId") Long issueId,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("tagId") && !parameters.containsKey("tagDescription")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("S'ha d'indicar el camp 'tagId' o 'tagDescription'")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String tagId = parameters.getFirst("tagId");
			String tagDescription = parameters.getFirst("tagDescription");

			final AbstractIssue oldIssue = (AbstractIssue)SerializationUtils.clone(issueSearchService.findIssueById(issueId));
			if(tagId!=null){
				issueAdministrationService.tagIssue(issueId, Long.valueOf(tagId));
			}
			else{
				List<Tag> tags = coreAdministrationService.findTagsByName(tagDescription);
		        Tag tag;
		        // Crear un nou tag
		        if(tags.isEmpty()){
		            tag = coreAdministrationService.createTag(tagDescription, false);
		        }
		        // En cas que existeixi un tag amb la descripció proporcionada, s'agafa la primer coincidència
		        else{
		            tag = tags.get(0);
		        }
		        // S'associa el tag a la incidència
		        issueAdministrationService.tagIssue(issueId, tag.getId());
			}
			final AbstractIssue issue = issueSearchService.findIssueById(issueId);
	        
	        // Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_ATTACH, issueId.toString(), username, AuditConstants.RESULT_CODE_OK, null);
	        
	        try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ISSUE_UPDATED, 
						"Incidència actualitzada: " + oldIssue.getVisibleId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new IssueDTOAdapter(oldIssue));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new IssueDTOAdapter(issue));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció d'incidència: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.CREATED).build();
		}
		catch(Exception e){
			log.error("Error etiquetant incidència: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_ATTACH, issueId.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error etiquetant la incidència")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{issueId}/tag/{tagId}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response dettachIssueTag(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("issueId") Long issueId, 
			@PathParam("tagId") Long tagId)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final AbstractIssue oldIssue = (AbstractIssue)SerializationUtils.clone(issueSearchService.findIssueById(issueId));
			issueAdministrationService.dettachIssueTag(Long.valueOf(issueId), Long.valueOf(tagId));
			final AbstractIssue issue = issueSearchService.findIssueById(issueId);
			
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_DETTACH, issueId.toString(), username, AuditConstants.RESULT_CODE_OK, null);
	        
	        try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_ISSUE_UPDATED, 
						"Incidència actualitzada: " + oldIssue.getVisibleId(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new IssueDTOAdapter(oldIssue));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new IssueDTOAdapter(issue));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció d'incidència: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant esborrant etiqueta de la incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_DETTACH, issueId.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit esborrant esborrant etiqueta de la incidència " + issueId)
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	/*---------------------------------*
     *             Alertes             *
     *---------------------------------*/
	@GET
	@Path("/{issueId}/alerts.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IIssueAlertDTO> getIssueAlerts(@PathParam("issueId") Long issueId)
	{
		List<IIssueAlertDTO> resultList = new ArrayList<IIssueAlertDTO>();
		
		List<IssueAlert> alerts = issueSearchService.findIssueAlertsByIssueId(issueId);
		if(alerts!=null){			
			for(IssueAlert alert:alerts){
				resultList.add(new IssueAlertDTOAdapter(alert));
			}
		}
		
		return resultList;
	}
	
	@DELETE
	@Path("/{issueId}/alert/{alertId}")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response deleteIssueAlert(@PathParam("issueId") Long id, @PathParam("alertId") Long alertId)
	{
		try{
			issueAdministrationService.deleteIssueAlert(alertId);
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant alerta: {}", e.getMessage(), e);
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant alerta")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	/*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
	@GET
	@Path("/{issueId}/customValues.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<ICustomValueDTO> getCustomValues(@PathParam("issueId") Long issueId)
	{
		List<ICustomValueDTO> resultList = new ArrayList<ICustomValueDTO>();
		
		List<IssueCustomValue> values = issueSearchService.findIssueCustomValues(issueId);
		if(values!=null){			
			for(IssueCustomValue value:values){
				resultList.add(new CustomValueDTOAdapter(value));
			}
		}
		
		return resultList;
	}
	
	/*---------------------------------*
     *      Registres de treball       *
     *---------------------------------*/
	@GET
	@Path("/{issueId}/workRegistries.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<IWorkRegistryDTO> getWorkRegistries(@PathParam("issueId") Long issueId)
	{
		List<IWorkRegistryDTO> resultList = new ArrayList<IWorkRegistryDTO>();
		
		AbstractIssue issue = issueSearchService.findIssueById(issueId);
		List<IssueWorkRegistry> registries = issueWorkService.findAllIssueWorkRegistriesByIssue(issue);
		if(registries!=null){			
			for(IssueWorkRegistry registry:registries){
				resultList.add(new WorkRegistryDTOAdapter(registry));
			}
		}
		
		return resultList;
	}
	
	@GET
	@Path("/{issueId}/workRegistry/{registryId}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IWorkRegistryDTO getWorkRegistryById(@Context HttpServletResponse response, 
			@PathParam("issueId") Long issueId,
			@PathParam("registryId") Long registryId)
	{
		IssueWorkRegistry registry = issueWorkService.findIssueWorkRegistryById(registryId);
		
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(registry==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new WorkRegistryDTOAdapter(registry);
	}
	
	@POST
	@Path("/{issueId}/workRegistry")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response createWorkRegistry(@Context HttpServletRequest request,
			@PathParam("issueId") Long issueId,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("workingDay")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'workingDay' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("username")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'username' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("time")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'time' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String strWorkingDay = parameters.getFirst("workingDay");
			Date workingDay = new SimpleDateFormat("dd/MM/yyyy").parse(strWorkingDay);
			Boolean includeInSprint = Boolean.valueOf(parameters.getFirst("includeInSprint"));
			String description = parameters.getFirst("description");
			String registryUsername = parameters.getFirst("username");
			User registryUser = userManagementService.findUserByUsername(registryUsername);
			Integer time = Integer.parseInt(parameters.getFirst("time"));
			
			AbstractIssue issue = issueSearchService.findIssueById(issueId);
			IssueWorkRegistry registry = issueWorkService.createIssueWorkRegistry(issue, workingDay, includeInSprint, description, registryUser, time);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORK_REGISTRY_CREATE, registry.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(registry.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant registre de treball: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORK_REGISTRY_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant el registre de treball")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{issueId}/workRegistry/{registryId}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response updateWorkRegistry(@Context HttpServletRequest request,
			@PathParam("issueId") Long issueId,
			@PathParam("registryId") Long registryId,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("workingDay")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'workingDay' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("username")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'username' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("time")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'time' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String strWorkingDay = parameters.getFirst("workingDay");
			Date workingDay = new SimpleDateFormat("").parse(strWorkingDay);
			Boolean includeInSprint = Boolean.valueOf(parameters.getFirst("includeInSprint"));
			String description = parameters.getFirst("description");
			String registryUsername = parameters.getFirst("username");
			User registryUser = userManagementService.findUserByUsername(registryUsername);
			Integer time = Integer.parseInt(parameters.getFirst("time"));
			
			AbstractIssue issue = issueSearchService.findIssueById(issueId);
			IssueWorkRegistry registry = issueWorkService.updateIssueWorkRegistry(registryId, issue, workingDay, includeInSprint, description, registryUser, time);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORK_REGISTRY_UDPATE, registry.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant registre de treball: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORK_REGISTRY_UDPATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant el registre de treball")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{issueId}/workRegistry/{registryId}")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response deleteWorkRegistry(@PathParam("issueId") Long id, @PathParam("registryId") Long registryId)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			issueWorkService.deleteIssueWorkRegistry(registryId);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORK_REGISTRY_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant registre de treball: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORK_REGISTRY_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant el registre de treball")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	/*---------------------------------*
     *      	Registres W.I.P.       *
     *---------------------------------*/
	@GET
	@Path("/{issueId}/wipRegistry.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IIssueWipRegistryDTO getIssueWorkInProgressRegistry(@Context HttpServletResponse response, 
			@PathParam("issueId") Long issueId, 
			@QueryParam("username") String username)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(username==null){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'username' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		User user = userManagementService.findUserByUsername(username);
		IssueWipRegistry registry = issueWorkService.findIssueWorkInProgressRegistry(issueId, user);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(registry==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new IssueWipRegistryDTOAdapter(registry);
	}
	
	@GET
	@Path("/{issueId}/wipRegistry/{registryId}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public IIssueWipRegistryDTO getIssueWorkInProgressRegistry(@Context HttpServletResponse response, 
			@PathParam("issueId") Long issueId, 
			@PathParam("registryId") Long registryId)
	{
		IssueWipRegistry registry = issueWorkService.findIssueWorkInProgressRegistryById(registryId);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(registry==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new IssueWipRegistryDTOAdapter(registry);
	}
	
	@POST
	@Path("/{issueId}/wipRegistry")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response createIssueWorkInProgressRegistry(@Context HttpServletRequest request,
			@PathParam("issueId") Long issueId,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("username")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'username' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String registryUsername = parameters.getFirst("username");
			User registryUser = userManagementService.findUserByUsername(registryUsername);
			
			IssueWipRegistry registry = issueWorkService.createWorkInProgressRegistry(issueId, registryUser);
				
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(registry.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant registre WIP: {}", e.getMessage(), e);
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant el registre WIP")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{issueId}/wipRegistry")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response deleteIssueWorkInProgressRegistry(@PathParam("issueId") Long issueId,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("username")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'username' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String username = parameters.getFirst("username");
			User user = userManagementService.findUserByUsername(username);
			IssueWipRegistry wipRegistry = issueWorkService.findIssueWorkInProgressRegistry(issueId, user);
			issueWorkService.deleteWorkInProgressRegistry(wipRegistry.getId());
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant registre WIP: {}", e.getMessage(), e);
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant el registre WIP")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{issueId}/wipRegistry/{registryId}")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_REPORTER)
	public Response deleteIssueWorkInProgressRegistry(@PathParam("issueId") Long issueId, 
			@PathParam("registryId") Long registryId)
	{
		try{
			issueWorkService.deleteWorkInProgressRegistry(registryId);
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant registre WIP: {}", e.getMessage(), e);
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant el registre WIP")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
