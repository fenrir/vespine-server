package org.fenrir.vespine.web.controller.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.SerializationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.adapter.SeverityDTOAdapter;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.ProviderSeverity;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.service.IProviderAdministrationService;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.web.ApplicationConstants;
import org.fenrir.vespine.web.service.IBroadcastService;
import org.fenrir.vespine.core.SecurityConstants;

/**
 * TODO Javadoc
 * URL del controlador /rest/severity
 * 
 * La configuració dels servlets realitzada al mòdul Guice corresponent fa que per defecte 
 * les urls assignades a aquest controlador comencin per /rest, es per això que no s'ha d'afegir
 * aquest prefix a l'anotació @Path
 * @author Antonio Archilla Nava
 * @version v0.1.20140828
 */
@Path("/severity")
public class SeverityController 
{
	private static final Pattern SLA_TERM_PATTERN = Pattern.compile("\\d+[mwdh]");
	
	private final Logger log = LoggerFactory.getLogger(CategoryController.class);	
	
	@Inject 
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueSearchService issueSearchService;
	
	@Inject
	private IBroadcastService broadcastService;
	
	@Inject
	private IProviderAdministrationService providerAdministrationService;
	
	@Inject
	private IAuditService auditService;
	
	public void setIssueSearchService(IIssueSearchService issueSearchService)
	{
		this.issueSearchService = issueSearchService;
	}
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setProviderAdministrationService(IProviderAdministrationService providerAdministrationService)
	{
		this.providerAdministrationService = providerAdministrationService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setBroadcastService(IBroadcastService broadcastService)
	{
		this.broadcastService = broadcastService;
	}
	
	@GET
	@Path("/search.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public List<ISeverityDTO> getSeverities()
	{
		List<ISeverityDTO> resultList = new ArrayList<ISeverityDTO>();
		
		List<IssueSeverity> severities = coreAdministrationService.findAllSeverities();
		for(IssueSeverity elem:severities){
			resultList.add(new SeverityDTOAdapter(elem));
		}
		
		return resultList;
	}
	
	@GET
	@Path("/{severityId}/issues.count")
    @Produces(MediaType.TEXT_PLAIN)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public String countSeverityIssues(@Context HttpServletResponse response, @PathParam("severityId") Long severityId)
	{
		IssueSeverity severity = coreAdministrationService.findSeverityById(severityId);
		long issueCount = issueSearchService.countIssuesBySeverity(severity);
		return Long.toString(issueCount);
	}
	
	@GET
	@Path("/{id}.json")
    @Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER)
	public ISeverityDTO getSeverityById(@Context HttpServletResponse response, @PathParam("id") Long id)
	{
		IssueSeverity severity = coreAdministrationService.findSeverityById(id);
		// Si no s'ha trobat s'indica el codi d'estat a la resposta
		if(severity==null){
			// L'status code s'especifica automàticament amb 204 - No Content perquè es retorna null
			return null;
		}
		// S'indica a la resposta que s'ha trobat
		response.setStatus(HttpServletResponse.SC_OK);
		return new SeverityDTOAdapter(severity);
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response createSeverity(@Context HttpServletRequest request, 
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("slaPattern")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'periode SLA' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!SLA_TERM_PATTERN.matcher(parameters.getFirst("slaPattern")).matches()){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El format de la durada del periode SLA és incorrecte")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String slaPattern = parameters.getFirst("slaPattern");
			Set<ProviderSeverity> providerSeverities = new HashSet<ProviderSeverity>();
			List<String> providerIds = parameters.get("providerSeverity");
			if(providerIds!=null){			
				for(String elem:parameters.get("providerCategory")){
					ProviderSeverity providerSeverity = providerAdministrationService.findProviderSeverityById(Long.valueOf(elem));
					providerSeverities.add(providerSeverity);
				}
			}
			final IssueSeverity severity = coreAdministrationService.createIssueSeverity(name, slaPattern, providerSeverities);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_CREATE, severity.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_SEVERITY_CREATED, 
						"Severitat creada: " + severity.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new SeverityDTOAdapter(severity));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de creació de severitat: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append("/").append(severity.getId()).append(".json").toString());
			return Response.status(Response.Status.CREATED).location(url).build();
		}
		catch(Exception e){
			log.error("Error creant severitat: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_CREATE, null, username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error creant la severitat")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response updateSeverity(@Context HttpServletRequest request,
			@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator,
			@PathParam("id") Long id,
			MultivaluedMap<String, String> parameters)
	{
		/* Comprobació dels paràmetres obligatoris 
		 * Es llança un error en cas de ser incorrectes 
		 */
		if(!parameters.containsKey("name")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'nom' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!parameters.containsKey("slaPattern")){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El camp 'periode SLA' és obligatori")
					.type(MediaType.TEXT_PLAIN).build());
		}
		if(!SLA_TERM_PATTERN.matcher(parameters.getFirst("slaPattern")).matches()){
			throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
					.entity("El format de la durada del 'periode SLA' és incorrecte")
					.type(MediaType.TEXT_PLAIN).build());
		}
		
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			/* Es recuperen els paràmetres i es crida a la capa de negoci per
			 * que faci tota la feina 
			 */
			String name = parameters.getFirst("name");
			String slaPattern = parameters.getFirst("slaPattern");
			Set<ProviderSeverity> providerSeverities = new HashSet<ProviderSeverity>();
			List<String> providerIds = parameters.get("providerSeverity");
			if(providerIds!=null){			
				for(String elem:parameters.get("providerCategory")){
					ProviderSeverity providerSeverity = providerAdministrationService.findProviderSeverityById(Long.valueOf(elem));
					providerSeverities.add(providerSeverity);
				}
			}
			final IssueSeverity oldSeverity = (IssueSeverity)SerializationUtils.clone(coreAdministrationService.findSeverityById(id));
			final IssueSeverity severity = coreAdministrationService.updateIssueSeverity(id, name, slaPattern, providerSeverities);
				
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_UPDATE, severity.getId().toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_SEVERITY_UPDATED, 
						"Severitat actualitzant: " + oldSeverity.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new SeverityDTOAdapter(oldSeverity));
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE, new SeverityDTOAdapter(severity));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de modificanció de severitat: {}", e.getMessage(), e);
			}
			
			// Format de la resposta amb la url del projecte creat
			URI url = URI.create(request.getRequestURL().append(".json").toString());
			return Response.status(Response.Status.OK).location(url).build();
		}
		catch(Exception e){
			log.error("Error modificant severitat: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_UPDATE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error modificant la severitat")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Consumes("application/x-www-form-urlencoded")
	@RequiresRoles(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR)
	public Response deleteSeverity(@DefaultValue(ApplicationConstants.BROADCAST_ORIGINATOR_SYSTEM) @HeaderParam("originator") String originator, 
			@PathParam("id") Long id)
	{
		Subject currentUser = SecurityUtils.getSubject();
		String username = currentUser.getPrincipal().toString();
		try{
			final IssueSeverity oldSeverity = coreAdministrationService.findSeverityById(id);
			coreAdministrationService.deleteIssueSeverity(id);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_OK, null);
			
			try{
				broadcastService.sendMessage(ApplicationConstants.BROADCAST_CHANNEL_APPLICATION, 
						originator,
						BroadcastMessage.MESSAGE_TYPE_SEVERITY_DELETED, 
						"Severitat eliminat: " + oldSeverity.getName(), 
						new HashMap<String, Object>()
						{
							{
								put(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE, new SeverityDTOAdapter(oldSeverity));
							}
						}
				);
			}
			catch(Exception e){
				log.error("Error notificant missatge de d'eliminació de severitats: {}", e.getMessage(), e);
			}
			
			return Response.status(Response.Status.OK).build();
		}
		catch(Exception e){
			log.error("Error esborrant severitat: {}", e.getMessage(), e);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_DELETE, id.toString(), username, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new WebApplicationException(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("S'ha produit un error esborrant la severitat")
					.type(MediaType.TEXT_PLAIN).build());
		}
	}
}
