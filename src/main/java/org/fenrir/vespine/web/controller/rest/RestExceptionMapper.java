package org.fenrir.vespine.web.controller.rest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mapper d'errors de tipus {@link RuntimeException} que s'encarregarà de gestionar totes les excepcions
 * d'aquest tipus no capturades durant el transcurs d'una petició REST.<br/>
 * Queda automàticament resgistrat gràcies a l'anotació {@link Provider} i al fet que es
 * troba dins el package de controladors REST configurat al mòdul REST de Guice  
 * @see Provider
 * @author Antonio Archilla Nava
 * @version v0.1.20140812
 */
@Provider
public class RestExceptionMapper implements ExceptionMapper<RuntimeException> 
{
	private Logger log = LoggerFactory.getLogger(RestExceptionMapper.class);

	/**
	 * Mètode que s'executa si una petició REST genera una excepció i aquesta no és capturada<br/>
	 * S'encarrega de registrar el missatge de log corresponent a l'error i retornar al client una resposta
	 * entenedora indicant la causa de l'error. 
	 * @param e {@link RuntimeException} Excepció no capturada ocurreguda durant l'execució de la petició REST  
	 * @return {@link Response} Resposta que es retornarà al client indicant-ne l'error a
	 * 		través del codi <b>500 - Internal error</b> i un missatge descriptiu de la causa.
	 */
	public Response toResponse(RuntimeException e) 
	{
		log.error("Error inesperat: " + e.getMessage(), e);
		
		// Es necessari fer un defaultString perquè si no la resposta envia el missatge per defecte del contenidor.
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.entity(StringUtils.defaultIfEmpty(e.getMessage(), "Error inesperat"))
				.type(MediaType.TEXT_PLAIN).build();
	}
}