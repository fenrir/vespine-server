package org.fenrir.vespine.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.web.exception.ApplicationException;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140806
 */
public class ApplicationServletListener implements ServletContextListener 
{
	private final Logger log = LoggerFactory.getLogger(ApplicationServletListener.class);
	
	public void contextInitialized(ServletContextEvent event)
	{
		/* Càrrega de la configuració */
		Properties properties = new Properties();
		try{
			FileInputStream fis = new FileInputStream(new File(getClass().getResource("/org/fenrir/vespine/web/conf/application.properties").toURI()));
			properties.load(fis);			
			fis.close();
			
			// Recuperació ubicació a disc del workspace
			retrieveWorkspaceFSLocation(properties);
			
			// Creació de les bases de dades
			String userDatabaseUrl = properties.getProperty(ApplicationConstants.CONFIGURATION_USER_DATABASE_URL);
			createDatabase(userDatabaseUrl, "org/fenrir/vespine/web/conf/db_user_changelog_v1.xml");
			String applicationDatabaseUrl = properties.getProperty(ApplicationConstants.CONFIGURATION_DATA_DATABASE_URL);
			createDatabase(applicationDatabaseUrl, "org/fenrir/vespine/web/conf/db_data_changelog_v1.xml");
		}		
		catch(IOException e){
			// No s'hauria de donar mai
			log.error("Error en llegir les propietats d'arrancada: " + e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		catch(URISyntaxException e){
			// No s'hauria de donar mai
			log.error("Error en llegir les propietats d'arrancada: {}", e.getMessage(), e);
			throw new RuntimeException("Error al llegir les propietats d'arrancada: " + e.getMessage(), e);
		}
		catch(ApplicationException e){
			log.error("Error inicialitzant l'aplicació: {}", e.getMessage(), e);
			throw new RuntimeException("Error inicialitzant l'aplicació: " + e.getMessage(), e);
		}
	}
	
	public void contextDestroyed(ServletContextEvent event) 
	{
		
	}

	private void createDatabase(String databaseUrl, String changelogFile) throws ApplicationException
	{
		Connection connection = null;
		Liquibase liquibase = null;
		try{
			InitialContext cxt = new InitialContext();
			DataSource datasource = (DataSource)cxt.lookup(databaseUrl);
			if(datasource==null){
			   throw new NamingException("Error localitzant el datasource de la base de dades " + databaseUrl);
			}
			
			connection = datasource.getConnection();
			connection.setAutoCommit(false);
			
			Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
			liquibase = new Liquibase(changelogFile, new ClassLoaderResourceAccessor(), database);
			// Es pot especificat una etiqueta a mode de guard per llançar uns determinats changesets (mirar tag changeset en la definició xml)
			// TODO Parametritzar el contexts
			liquibase.update(new Contexts(new String[]{"base", "devel"}));
		}
		catch(NamingException e){
			log.error("Error localitzant el datasource de la base de dades {}: {}", new Object[]{databaseUrl, e.getMessage(), e});
			throw new ApplicationException("Error localitzant el datasource de la base de dades " + databaseUrl + " : " + e.getMessage(), e);
		}
		catch(SQLException e){
			log.error("Error en connectar a la base de dades {}: {}", new Object[]{databaseUrl, e.getMessage(), e});
			throw new ApplicationException("Error en connectar a la base de dades " + databaseUrl + " : " + e.getMessage(), e);
		}
		catch(LiquibaseException e){
			log.error("Error en actualitzar l'estructura de la base de dades {}: {}", new Object[]{databaseUrl, e.getMessage(), e});
			throw new ApplicationException("Error en actualitzar l'estructura de la base de dades " + databaseUrl + " : " + e.getMessage(), e);
		}
		finally{
			try{
				if(liquibase!=null){
					/* S'esborra el flag que indica que s'està actualitzant la BDD. Aixó permet que un posterior procés
					 * d'actualització pugui dur-se a terme
					 */
					liquibase.forceReleaseLocks();
				}
				if(connection!=null){
					connection.close();
				}
			}
			catch(SQLException e){
				log.error("Error tancant la connexió a la base de dades {}: {}", new Object[]{databaseUrl, e.getMessage(), e});
				throw new ApplicationException("Error tancant la connexió a la base de dades " + databaseUrl + " : " + e.getMessage(), e);
			}
			catch(LiquibaseException e){
				log.error("Error en actualitzar l'estructura de la base de dades {}: {}", new Object[]{databaseUrl, e.getMessage(), e});
				throw new ApplicationException("Error en actualitzar l'estructura de la base de dades " + databaseUrl + " : " + e.getMessage(), e);
			}
		}
	}
	
	private void retrieveWorkspaceFSLocation(Properties properties) throws ApplicationException
	{
		try{
			String locationKey = properties.getProperty(ApplicationConstants.CONFIGURATION_WORKSPACE_LOCATION);
			InitialContext cxt = new InitialContext();
			String location = (String)cxt.lookup(locationKey);
			
			if(StringUtils.isBlank(location)){
				log.error("No s'ha especificat valor al contexte JNDI per l'ubicació del workspace");
				throw new ApplicationException("No s'ha especificat valor al contexte JNDI per l'ubicació del workspace");
			}
			
			log.info("Recuperada ubicació del workspace al FS: {}", location);
			ApplicationContext.getInstance().setWorkspaceLocation(location);
		}
		catch(NamingException e){
			log.error("Error recuperant l'ubicació del workspace: {}", e.getMessage(), e);
			throw new ApplicationException("Error recuperant l'ubicació del workspace: " + e.getMessage(), e);
		}
	}
}
