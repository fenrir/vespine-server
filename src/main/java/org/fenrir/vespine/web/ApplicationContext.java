package org.fenrir.vespine.web;

import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import org.fenrir.vespine.spi.ApplicationContextHandler;
import org.fenrir.vespine.spi.IApplicationContext;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140105
 */
public class ApplicationContext implements IApplicationContext 
{
	private static ApplicationContext instance;
	
	private String workspaceLocation;
	private Injector injector;
	
	private ApplicationContext()
	{
		
	}
	
	public static ApplicationContext getInstance()
	{
		if(instance==null){
			instance = new ApplicationContext();
			// Es posa el nou context a disposició de la resta de mòduls
			ApplicationContextHandler.getInstance().setApplicationContext(instance);
		}
		
		return instance;
	}
	
	public void initialize(Injector injector)
	{
		this.injector = injector;
	}
	
	public String getWorkspaceLocation()
	{
		return workspaceLocation;
	}
	
	public void setWorkspaceLocation(String workspaceLocation)
	{
		this.workspaceLocation = workspaceLocation;
	}
	
	@Override
	public Object getRegisteredComponent(Class<?> type)
    {
        return injector.getInstance(type);        
    }
    
	@Override
    public Object getRegisteredComponent(Class<?> type, String name)
    {
        Key<?> key = Key.get(type, Names.named(name));        
        return injector.getInstance(key);
    }

    @Override
    public void injectMembers(Object object)
    {
        injector.injectMembers(object);
    }
}