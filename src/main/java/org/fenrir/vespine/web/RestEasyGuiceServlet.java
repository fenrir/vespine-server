package org.fenrir.vespine.web;

import java.lang.reflect.Type;
import javax.inject.Singleton;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.ext.Provider;
import org.jboss.resteasy.plugins.guice.GuiceResourceFactory;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;
import org.jboss.resteasy.spi.Registry;
import org.jboss.resteasy.spi.ResourceFactory;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.jboss.resteasy.util.GetRestful;
import com.google.inject.Binding;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20141026
 */
@SuppressWarnings("serial")
@Singleton
public class RestEasyGuiceServlet extends HttpServletDispatcher
{
	private static final Logger log = LoggerFactory.getLogger(RestEasyGuiceServlet.class);

    @Inject 
    private Injector injector;
    
    public void setInjector(Injector injector)
    {
    	this.injector = injector;
    }

	@Override
	public void init(ServletConfig servletConfig) throws ServletException 
	{
		super.init(servletConfig);
		
		ServletContext context = servletConfig.getServletContext();
        Registry registry = (Registry)context.getAttribute(Registry.class.getName());
        ResteasyProviderFactory providerFactory = (ResteasyProviderFactory)context.getAttribute(ResteasyProviderFactory.class.getName());

        // This code is copied wholesale as-is from (private) ModuleProcessor.processInjector()
        for(final Binding<?> binding : injector.getBindings().values()){
            final Type type = binding.getKey().getTypeLiteral().getType();
            if (type instanceof Class){
                final Class<?> beanClass = (Class<?>) type;
                if(GetRestful.isRootResource(beanClass)){
                    final ResourceFactory resourceFactory = new GuiceResourceFactory(binding.getProvider(), beanClass);
                    log.info("registering factory for {}", beanClass.getName());

                    registry.addResourceFactory(resourceFactory);
                }
                if(beanClass.isAnnotationPresent(Provider.class)){
                    log.info("registering provider instance for {}", beanClass.getName());

                    providerFactory.registerProviderInstance(binding.getProvider().get());
                }
            }
        }
	}
}
