package org.fenrir.vespine.web.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20121005
 */
@SuppressWarnings("serial")
public class ApplicationException extends Exception 
{
    public ApplicationException() 
    {
        super();	
    }

    public ApplicationException(String message, Throwable cause) 
    {
        super(message, cause);
    }

    public ApplicationException(String message) 
    {
        super(message);	
    }

    public ApplicationException(Throwable cause) 
    {
        super(cause);	
    }
}