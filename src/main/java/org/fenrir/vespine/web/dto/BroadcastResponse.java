package org.fenrir.vespine.web.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BroadcastResponse 
{
	@XmlElement
	private String from;
	@XmlElement
	private String message;
	
	public String getFrom() 
	{
		return from;
	}
	
	public void setFrom(String from) 
	{
		this.from = from;
	}
	
	public String getMessage() 
	{
		return message;
	}
	
	public void setMessage(String message) 
	{
		this.message = message;
	}
}
