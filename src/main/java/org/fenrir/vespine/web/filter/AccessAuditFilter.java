package org.fenrir.vespine.web.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import com.google.inject.Singleton;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140829
 */
@Singleton
public class AccessAuditFilter implements Filter 
{
	private final Logger log = LoggerFactory.getLogger(AccessAuditFilter.class);
	
	private FilterConfig filterConfig;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException 
	{
		this.filterConfig = filterConfig;
	}
	
	@Override
	public void destroy() 
	{
		// Res a fer...
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException 
	{
		Subject subject = SecurityUtils.getSubject();
		String principal = null;
		if(subject!=null && subject.getPrincipal()!=null){
			principal = subject.getPrincipal().toString();
		}
		String remoteAddr = request.getRemoteAddr();
		String url = null;
		String operation = null;
		if(request instanceof HttpServletRequest){
			url = ((HttpServletRequest)request).getRequestURL().toString();
			operation = ((HttpServletRequest)request).getMethod();
		}
		if(log.isDebugEnabled()){
			log.info("Capturat accés {} {} desde {} amb l'usuari {}", new Object[]{operation, url, remoteAddr, principal});
		}
		
		// TODO Guardar a bdd
		
		chain.doFilter(request, response);
	}
}
