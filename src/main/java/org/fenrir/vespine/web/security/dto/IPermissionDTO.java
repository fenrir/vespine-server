package org.fenrir.vespine.web.security.dto;

public interface IPermissionDTO 
{
	public String getValue();
	public String getDescription();
}
