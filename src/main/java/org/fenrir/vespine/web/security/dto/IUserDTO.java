package org.fenrir.vespine.web.security.dto;

import java.util.List;

public interface IUserDTO 
{
	public String getUsername();
	public List<IRoleDTO> getRoles();
	public List<IPermissionDTO> getPermissions();
}
