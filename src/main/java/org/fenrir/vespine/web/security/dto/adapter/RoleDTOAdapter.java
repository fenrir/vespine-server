package org.fenrir.vespine.web.security.dto.adapter;

import org.fenrir.vespine.web.security.domain.Role;
import org.fenrir.vespine.web.security.dto.IRoleDTO;
import org.fenrir.vespine.web.security.dto.IUserDTO;

public class RoleDTOAdapter implements IRoleDTO 
{
	private Role role;
	
	public RoleDTOAdapter(Role role)
	{
		this.role = role;
	}
	
	@Override
	public String getName() 
	{
		return role.getName();
	}

	@Override
	public String getDescription() 
	{
		return role.getDescription();
	}
	
	@Override
	public String toString()
	{
		return role.toString();
	}
	
	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IUserDTO)){
            return false;
        }
        IRoleDTO other = (IRoleDTO) obj;
        if(getName()==null){
            if(other.getName()!=null){
                return false;
            }
        }
        else if(!getName().equals(other.getName())){
            return false;
        }

        return true;
    }
}
