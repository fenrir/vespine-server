package org.fenrir.vespine.web.security.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.fenrir.vespine.web.security.dao.IPermissionDAO;
import org.fenrir.vespine.web.security.domain.Permission;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140728
 */
public class PermissionDAOImpl implements IPermissionDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
    @Override
	@Transactional
	public List<Permission> findAllPermissions() 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Permission", Permission.class).getResultList();
	}

    @Override
	@Transactional
	public Permission findPermissionById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(Permission.class, id);
	}

    @Override
	@Transactional
	public Permission findPermissionByName(String name) 
	{
		EntityManager em = entityManagerProvider.get();
        List<Permission> resultList = em.createQuery("from Permission where name=:name", Permission.class)
            .setParameter("name", name)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}

	@Override
	@Transactional
	public Permission createPermission(Permission permission) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(permission);
		
        return permission;
	}

	@Override
	@Transactional
	public Permission updatePermission(Permission permission) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(permission);
	}

	@Override
	@Transactional
	public void deletePermission(Permission permission) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(permission);
	}
}
