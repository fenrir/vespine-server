package org.fenrir.vespine.web.security.listener;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationListener;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131230
 */
public class AccessListener implements AuthenticationListener 
{

	@Override
	public void onFailure(AuthenticationToken token, AuthenticationException exception) 
	{
		// TODO Auto-generated method stub
		System.out.println("Failure!!");
	}

	@Override
	public void onLogout(PrincipalCollection principals) 
	{
		// TODO Auto-generated method stub
		System.out.println("Logout!!");
	}

	@Override
	public void onSuccess(AuthenticationToken token, AuthenticationInfo info) 
	{
		// TODO Auto-generated method stub
		System.out.println("Success!!");
	}
}
