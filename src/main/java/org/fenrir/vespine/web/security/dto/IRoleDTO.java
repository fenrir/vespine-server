package org.fenrir.vespine.web.security.dto;

public interface IRoleDTO 
{
	public String getName();
	public String getDescription();
}
