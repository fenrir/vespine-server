package org.fenrir.vespine.web.security.dao;

import java.util.List;
import org.fenrir.vespine.web.security.domain.Role;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140728
 */
public interface IRoleDAO 
{
	public List<Role> findAllRoles();
	public Role findRoleById(Long id);
	public Role findRoleByName(String name);
	
	public Role createRole(Role role);
	public Role updateRole(Role role);
	public void deleteRole(Role role);
}
