package org.fenrir.vespine.web.security.dto.adapter;

import java.util.ArrayList;
import java.util.List;
import org.fenrir.vespine.web.security.domain.Permission;
import org.fenrir.vespine.web.security.domain.Role;
import org.fenrir.vespine.web.security.domain.User;
import org.fenrir.vespine.web.security.dto.IPermissionDTO;
import org.fenrir.vespine.web.security.dto.IRoleDTO;
import org.fenrir.vespine.web.security.dto.IUserDTO;

public class UserDTOAdapter implements IUserDTO 
{
	private User user;
	
	public UserDTOAdapter(User user)
	{
		this.user = user;
	}

	@Override
	public String getUsername() 
	{
		return user.getName();
	}

	@Override
	public List<IRoleDTO> getRoles() 
	{
		List<IRoleDTO> resultList = new ArrayList<IRoleDTO>();
		for(Role role:user.getRoles()){
			resultList.add(new RoleDTOAdapter(role));
		}
		
		return resultList; 
	}

	@Override
	public List<IPermissionDTO> getPermissions() 
	{
		List<IPermissionDTO> resultList = new ArrayList<IPermissionDTO>();
		
		List<Permission> permissions = new ArrayList<Permission>();
		for(Role role:user.getRoles()){
			for(Permission permission:role.getPermissions()){
				if(!permissions.contains(permission)){
					permissions.add(permission);
				}
			}
		}
		
		for(Permission permission:permissions){
			resultList.add(new PermissionDTOAdapter(permission));
		}
		
		return resultList;
	}
	
	@Override
	public String toString()
	{
		return user.toString();
	}
	
	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IUserDTO)){
            return false;
        }

        IUserDTO other = (IUserDTO) obj;
        if(getUsername()==null){
            if(other.getUsername()!=null){
                return false;
            }
        }
        else if(!getUsername().equals(other.getUsername())){
            return false;
        }

        return true;
    }
}
