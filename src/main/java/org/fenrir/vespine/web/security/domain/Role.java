package org.fenrir.vespine.web.security.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140728
 */
@Entity
@Table(name="APP_ROLE")
public class Role 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Version
    private Long version;
    
    @Column(unique=true)
    private String name;
    
    private String description;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "USER_ROLES",
        joinColumns = @JoinColumn(name="ROLE_ID", referencedColumnName="ID"),
        inverseJoinColumns = @JoinColumn(name="USER_ID", referencedColumnName="ID")
    )
    private Collection<User> users;
    
    /**
     * TODO Mirar si hi ha forma de crear un proxy per obtenir la col.lecció a fi de que sigui LAZY
     * Les col.leccions s'han de posar en mode fetch=EAGER perquè es pot donar el cas que 
     * el get es faci en un entorn no transaccional, el que produeix un error.
     */
    @ManyToMany(mappedBy="roles", fetch=FetchType.EAGER)
    private Set<Permission> permissions;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public Long getVersion() 
	{
		return version;
	}

	public void setVersion(Long version) 
	{
		this.version = version;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	public Collection<User> getUsers()
	{
		return users;
	}
	
	public void setUsers(Collection<User> users)
	{
		this.users = users;
	}
	
	public Set<Permission> getPermissions()
	{
		if(permissions==null){
			return Collections.emptySet();
		}
		return permissions;
	}
	
	public void setPermissions(Set<Permission> permissions)
	{
		this.permissions = permissions;
	}

	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) 
	{
		this.lastUpdated = lastUpdated;
	}
	
	@Override
    public String toString()
    {
    	return name;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        Role other = (Role) obj;
        /* Name */
        if(name==null){
            if(other.name!=null){
                return false;
            }
        } 
        else if(!name.equals(other.name)){
            return false;
        }
        
        return true;
    }
}
