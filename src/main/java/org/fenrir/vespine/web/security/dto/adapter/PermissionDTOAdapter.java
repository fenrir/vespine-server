package org.fenrir.vespine.web.security.dto.adapter;

import org.fenrir.vespine.web.security.domain.Permission;
import org.fenrir.vespine.web.security.dto.IPermissionDTO;
import org.fenrir.vespine.web.security.dto.IUserDTO;

public class PermissionDTOAdapter implements IPermissionDTO 
{
	private Permission permission;
	
	public PermissionDTOAdapter(Permission permission)
	{
		this.permission = permission;
	}
	
	@Override
	public String getValue() 
	{
		return permission.getValue();
	}

	@Override
	public String getDescription() 
	{
		return permission.getDescription();
	}

	@Override
	public String toString()
	{
		return permission.toString();
	}
	
	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getValue() == null) ? 0 : getValue().hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(!(obj instanceof IUserDTO)){
            return false;
        }

        IPermissionDTO other = (IPermissionDTO) obj;
        if(getValue()==null){
            if(other.getValue()!=null){
                return false;
            }
        }
        else if(!getValue().equals(other.getValue())){
            return false;
        }

        return true;
    }
}
