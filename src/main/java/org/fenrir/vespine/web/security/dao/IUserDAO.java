package org.fenrir.vespine.web.security.dao;

import java.util.List;
import org.fenrir.vespine.web.security.domain.User;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140728
 */
public interface IUserDAO 
{
	public List<User> findAllUsers();
	public User findUserById(Long id);
	public User findUserByName(String name);
	
	public User createUser(User user);
	public User updateUser(User user);
	public void deleteUser(User user);
}
