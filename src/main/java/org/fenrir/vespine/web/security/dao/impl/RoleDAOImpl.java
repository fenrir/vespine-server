package org.fenrir.vespine.web.security.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.web.security.dao.IRoleDAO;
import org.fenrir.vespine.web.security.domain.Role;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140728
 */
public class RoleDAOImpl implements IRoleDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
    @Override
	@Transactional
	public List<Role> findAllRoles() 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from Role", Role.class).getResultList();
	}

    @Override
	@Transactional
	public Role findRoleById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(Role.class, id);
	}

    @Override
	@Transactional
	public Role findRoleByName(String name) 
	{
		EntityManager em = entityManagerProvider.get();
        List<Role> resultList = em.createQuery("from Role where name=:name", Role.class)
            .setParameter("name", name)
            .getResultList();
        
        /* Si es crida directament getSingleResult() sobre la query i aquesta retorna 0 resultats, aquest llança excepció
         * Fent-ho d'aquesta manera s'asegura que en cas de no trobar res retorni null en comptes de dónar error
         */
        if(resultList.isEmpty()){
            return null;
        }
        else{
            return resultList.get(0);
        }
	}

	@Override
	@Transactional
	public Role createRole(Role user) 
	{
		EntityManager em = entityManagerProvider.get();
        em.persist(user);
		
        return user;
	}

	@Override
	@Transactional
	public Role updateRole(Role user) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(user);
	}

	@Override
	@Transactional
	public void deleteRole(Role user) 
	{
		EntityManager em = entityManagerProvider.get();
		em.remove(user);
	}
}
