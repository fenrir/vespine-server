package org.fenrir.vespine.web.security.dao;

import java.util.List;
import org.fenrir.vespine.web.security.domain.Permission;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140728
 */
public interface IPermissionDAO 
{
	public List<Permission> findAllPermissions();
	public Permission findPermissionById(Long id);
	public Permission findPermissionByName(String name);
	
	public Permission createPermission(Permission permission);
	public Permission updatePermission(Permission permission);
	public void deletePermission(Permission permission);
}
