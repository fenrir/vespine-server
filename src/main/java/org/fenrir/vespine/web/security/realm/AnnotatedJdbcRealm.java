package org.fenrir.vespine.web.security.realm;

import javax.inject.Inject;
import javax.sql.DataSource;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import com.google.inject.name.Named;

/**
 * TODO Documentació
 * Adaptació de la classe org.apache.shiro.realm.jdbc.JdbcRealm per permetre l'injecció 
 * de la configuració a través de Guice.
 * @author Antonio Archilla Nava
 * @version v0.1.20131123
 */
public class AnnotatedJdbcRealm extends JdbcRealm 
{
	@Inject
	@Override
	public void setDataSource(DataSource dataSource) 
	{
		super.setDataSource(dataSource);
	}

	@Inject
	@Override
	public void setAuthenticationQuery(@Named("shiro.authenticationQuery")String authenticationQuery) 
	{
		super.setAuthenticationQuery(authenticationQuery);
	}

	@Inject
	@Override
	public void setUserRolesQuery(@Named("shiro.userRolesQuery")String userRolesQuery) 
	{
		super.setUserRolesQuery(userRolesQuery);
	}

	@Inject
	@Override
	public void setPermissionsQuery(@Named("shiro.permissionsQuery")String permissionsQuery) 
	{
		super.setPermissionsQuery(permissionsQuery);
	}

	@Inject
	@Override
	public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) 
	{
		super.setCredentialsMatcher(credentialsMatcher);
	}
}