package org.fenrir.vespine.web;

/**
 * Classe que conté la definició de les constants genèriques de l'aplicació.
 * S'hi defineixen els noms de les propietats ubicades al fitxer application.properties i
 * els IDs utilitzats per la injecció de depedències d'algunes classes.
 * @author Antonio Archilla Nava
 * @version v0.1.20140815
 */
public class ApplicationConstants 
{
	/* Unitat de persistència de l'aplicació */
	/**
	 * Constant que correspon a la ID de la unitat de persistència JPA de l'aplicació 
	 */
	public static final String PERSISTENCE_UNIT_APPLICATION = "applicationJPA";
	
	/**
	 * Constant que correspon a la ID de la unitat de persistència JPA de la BDD d'usuaris
	 */
	public static final String PERSISTENCE_UNIT_USERS = "usersJPA";
	
	/* IDs per la injecció de dependències */
	/** 
	 * Constant que correspon a la ID en el sistema d'injecció de dependencies de l'objecte
	 * que emmagatzema les propietats de configuració de l'aplicació.
	 */
	public static final String NAMED_INJECTION_APPLICATION_PROPERTIES = "applicationProperties";
	
	/* Propietats de l'aplicació */
	/**
	 * Constant que correspon a la URL de connexió JNDI al datasource de la base de dades a on s'emmagatzemen els usuaris, els rols i els seus permisos.
	 * El seu valor per defecte és java:/comp/env/jdbc/vespine-users
	 */
	public static final String CONFIGURATION_USER_DATABASE_URL = "user.database.url";
	
	/**
	 * Constant que correspon a la URL de connexió JNDI al datasource de la base de dades a on s'emmagatzemen les dades de l'aplicació.
	 * El seu valor per defecte és java:/comp/env/jdbc/vespine-data
	 */
	public static final String CONFIGURATION_DATA_DATABASE_URL = "data.database.url";
	
	/**
	 * Constant que correspon a la URL de connexió JNDI a la ubicació al FS del workspace de l'aplicació
	 */
	public static final String CONFIGURATION_WORKSPACE_LOCATION = "workspace.fs.location";
	
	/* Constants sistema Broadcast */
	/**
	 * ID del broadcaster corresponent al canal de comunicació de missatges de sistema
	 */
	public static final String BROADCAST_CHANNEL_SYSTEM = "channel:system";
	
	/**
	 * ID del broadcaster corresponent al canal de comunicació de missatges d'aplicació
	 */
	public static final String BROADCAST_CHANNEL_APPLICATION = "channel:application";
	
	/**
	 * ID de l'originador de missatges broadcast iniciats des del sistema
	 * També s'indicarà aquest valor en el cas que no arribi des de l'aplicaicó origen de l'acció
	 */
	public static final String BROADCAST_ORIGINATOR_SYSTEM = "system";
}
